include "bean.thrift"

namespace java com.diwi.gothrift.test.service

service VideoService {
    bean.VideoPO get(1: i32 id);
    list<bean.VideoPO> gets(1: i32 id);
}