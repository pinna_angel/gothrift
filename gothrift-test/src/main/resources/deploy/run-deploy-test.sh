#!/bin/bash
base="${deploy.base}"
name="${project.name}"
java_home="${deploy.java.home}"
bin_home="${deploy.bin.home}"
conf_home="${deploy.conf.home}/resin"
deploy="${deploy.base}"
webapp="${deploy}/${project.name}"
servers=(${deploy.mmsprovider.server})

serverCount=${#servers[@]}
serial_no=`date +%s`
i=0
while [ $i -lt $serverCount ]
do
    server=${servers[$i]}
    echo "deploy $name on $server"
    host=`echo $server | cut -d: -f1`
    port=`echo $server | cut -d: -f2`

    log_home="${deploy.log.home}/resin/${project.name}-$port"
    shell="$bin_home/resin-$name-$port.sh"
    conf="$conf_home/resin-$name-$port.xml"

    ssh root@$host "mkdir -p $bin_home"
    scp *.sh root@$host:$bin_home
    ssh root@$host "rm -f $bin_home/run-deploy*.sh && chmod +x $bin_home/*.sh"
    ssh root@$host "mv -f $bin_home/resin-$name.sh $shell"
    ssh root@$host "chown root $shell"
    ssh root@$host "echo $conf >> $shell"

    ssh root@$host "mkdir -p $conf_home"
    ssh root@$host "chown -R root $conf_home"
    scp resin-$name.xml root@$host:$conf
    ssh root@$host "sed -i 's/##port##/$port/' $conf"

    ssh root@$host "mkdir -p $log_home/bak"
    ssh root@$host "chown -R root $log_home"

    ssh root@$host "$shell stop" || echo "$shell is not running"

    if [ ! -s $deploy/$name-$serial_no.war ] ; then
        ssh root@$host "mkdir -p $webapp"
        ssh root@$host "chown -R root $deploy"

        scp artifacts/$name.war root@$host:$deploy
        ssh root@$host "cd $webapp && rm -rf * && $java_home/bin/jar xf $deploy/$name.war"
        

        #special begin
        # ssh resin@$host "[ -d $deploy/frag/inc ] || (mkdir -p $deploy/frag/inc && cp -r $webapp/frag/inc/* $deploy/frag/inc/)"
        # ssh resin@$host "rm -fr $webapp/frag/inc && ln -s $deploy/frag/inc $webapp/frag/inc"
        #special end

		ssh root@$host "mkdir -p $deploy/bak"
        ssh root@$host "cp $deploy/$name.war $deploy/$name-$serial_no.war &&  mv $deploy/$name-$serial_no.war $deploy/bak/"
    fi

    while [ `ssh root@$host "ps -ef | grep $conf | grep -v grep | wc -l"` -gt 0 ]
    do
	    echo "waiting for $shell resin stop"
    	sleep 1
    done

    ssh root@$host "$shell start"
    sleep 10
    i=$((i+1))

done
echo "all done."
