package com.diwi.gothrift.test.utils;

import static com.diwi.gothrift.core.utils.Constants.SLASH;

import com.diwi.gothrift.test.service.HelloWorldService;
import com.diwi.gothrift.test.service.ThriftTestService;

/**
 * @author dingwei
 * @since 16/6/16.
 */
public class Constants {
    public static String helloWorldService1 = SLASH + HelloWorldService.Iface.class.getName() + SLASH + "1.0";
    public static String helloWorldService2 = SLASH + HelloWorldService.Iface.class.getName() + SLASH + "2.0";
    public static String thriftTestService = SLASH + ThriftTestService.Iface.class.getName() + SLASH + "1.0";
}
