package com.diwi.gothrift.test.bean;

import java.io.Serializable;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map;

import org.apache.commons.collections.MapUtils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.TypeReference;

/**
 * @author dingwei
 * @since 15/10/13.
 */
public class VideoPO implements Serializable {
    private static final long serialVersionUID = 8529934938839558851L;

    private long id;

    private Map<String, String> sourceId;
    private Map<String, String> videoType;
    private Long pid;
    private Integer porder = 0;
    private String mid;

    private String nameCn;
    private String namePinyinAbb;
    private String alias;
    private String subTitle;
    private String shortDesc;
    private String description;

    private String tag;

    private Map<String, String> category;
    private Map<String, String> subCategory;
    private String episode = "";
    private Integer btime = 0;
    private Integer etime = 0;

    private String adPoint;

    private Map<String, String> downloadPlatform;
    private Map<String, String> playPlatform;
    private Map<String, String> payPlatform;
    private Integer duration = 0;
    private String viki;
    private Integer deleted = 0;

    private String remark;
    private Float score;
    private String relativeContent;
    private String releaseDate;
    private String firstPlayTime;

    private Map<String, String> area;
    private String school;

    private LinkedHashMap<String, String> starring;
    private String startringPlay;
    private LinkedHashMap<String, String> directory;
    private Map<String, String> actor;
    private String actorPlay;

    private String officialUrl;
    private Map<String, String> language;
    private String singleName;
    private Map<String, String> style;
    private Map<String, String> instructor;
    private Map<String, String> compere;
    private Map<String, String> guest;

    private String singer;
    private Map<String, String> singerType;
    private String musicAuthors;
    private String maker;
    private String recordCompany;
    private String issueCompany;

    private Map<String, String> team;
    private Map<String, String> pre;
    private String quality;
    private Integer isHomemade = 0;
    private Integer direction = 0;

    private int logoNum;

    private Map<String, String> playTv;

    private Integer playControlPlatformCntv = 0;
    private Integer playControlPlatformHuashu = 0;
    private Integer playControlPlatformGuoguang = 0;

    private Date createTime;
    private Date updateTime;
    private Long userId;

    private Map<String, String> folkartType;
    private String videoPic;

    private Map<String, String> fitAge;

    private Map<String, String> contentRating;
    private Integer canSearch = 0;

    private Map<String, String> recommend;

    private String activityId;

    private Map<String, String> drmFlag;

    private Map<String, String> site;
    private int isFirstLook;

    private Map<String, Object> extData;
    private String coopPlatform;
    private String leId;
    private String autoVideoPic;
    private Integer isPay = 0;

    private Map<String, String> picAll;

    private Integer dolbyFlag = 0;

    private String vtypeFlag;

    private Integer vikiFlag = 0;

    private Integer matchId = 0;

    private String cutoffPlatform;

    private String cutoffTime;

    private Integer isPlayLock = 0;

    private Integer isBigPic = 0;

    private Integer isDanmaku = 0;

    private Long liveId;

    private Map<String, String> platformFirstOnTime;

    private Map<String, String> subtitles;

    private Map<String, String> audioTracks;

    private Map<String, String> defAudioTrack;

    private String defSubTrack;

    private Map<String, String> platformPreVid;

    private Map<String, String> platformNextVid;

    private String audioInfo;
    private String copyrightCode;

    private String relPositiveAlbumIds;

    private String shareCopy;

    private Integer isVipDownload = 0;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Map<String, String> getSourceId() {
        return sourceId;
    }

    public void setSourceId(Map<String, String> sourceId) {
        this.sourceId = sourceId;
    }

    public Map<String, String> getVideoType() {
        return videoType;
    }

    public void setVideoType(Map<String, String> videoType) {
        this.videoType = videoType;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public Integer getPorder() {
        return porder;
    }

    public void setPorder(Integer porder) {
        this.porder = porder;
    }

    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public String getNameCn() {
        return nameCn;
    }

    public void setNameCn(String nameCn) {
        this.nameCn = nameCn;
    }

    public String getNamePinyinAbb() {
        return namePinyinAbb;
    }

    public void setNamePinyinAbb(String namePinyinAbb) {
        this.namePinyinAbb = namePinyinAbb;
    }

    public String getAlias() {
        return alias;
    }

    public void setAlias(String alias) {
        this.alias = alias;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        this.subTitle = subTitle;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public Map<String, String> getCategory() {
        return category;
    }

    public void setCategory(Map<String, String> category) {
        this.category = category;
    }

    public Map<String, String> getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(Map<String, String> subCategory) {
        this.subCategory = subCategory;
    }

    public String getEpisode() {
        return episode;
    }

    public void setEpisode(String episode) {
        this.episode = episode;
    }

    public Integer getBtime() {
        return btime;
    }

    public void setBtime(Integer btime) {
        this.btime = btime;
    }

    public Integer getEtime() {
        return etime;
    }

    public void setEtime(Integer etime) {
        this.etime = etime;
    }

    public String getAdPoint() {
        return adPoint;
    }

    public void setAdPoint(String adPoint) {
        this.adPoint = adPoint;
    }

    public Map<String, String> getDownloadPlatform() {
        return downloadPlatform;
    }

    public void setDownloadPlatform(Map<String, String> downloadPlatform) {
        this.downloadPlatform = downloadPlatform;
    }

    public Map<String, String> getPlayPlatform() {
        return playPlatform;
    }

    public void setPlayPlatform(Map<String, String> playPlatform) {
        this.playPlatform = playPlatform;
    }

    public Map<String, String> getPayPlatform() {
        return payPlatform;
    }

    public void setPayPlatform(Map<String, String> payPlatform) {
        this.payPlatform = payPlatform;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getViki() {
        return viki;
    }

    public void setViki(String viki) {
        this.viki = viki;
    }

    public Integer getDeleted() {
        return deleted;
    }

    public void setDeleted(Integer deleted) {
        this.deleted = deleted;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Float getScore() {
        return score;
    }

    public void setScore(Float score) {
        this.score = score;
    }

    public String getRelativeContent() {
        return relativeContent;
    }

    public void setRelativeContent(String relativeContent) {
        this.relativeContent = relativeContent;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getFirstPlayTime() {
        return firstPlayTime;
    }

    public void setFirstPlayTime(String firstPlayTime) {
        this.firstPlayTime = firstPlayTime;
    }

    public Map<String, String> getArea() {
        return area;
    }

    public void setArea(Map<String, String> area) {
        this.area = area;
    }

    public String getSchool() {
        return school;
    }

    public void setSchool(String school) {
        this.school = school;
    }

    public LinkedHashMap<String, String> getStarring() {
        return starring;
    }

    public void setStarring(LinkedHashMap<String, String> starring) {
        this.starring = starring;
    }

    public String getStartringPlay() {
        return startringPlay;
    }

    public void setStartringPlay(String startringPlay) {
        this.startringPlay = startringPlay;
    }

    public LinkedHashMap<String, String> getDirectory() {
        return directory;
    }

    public void setDirectory(LinkedHashMap<String, String> directory) {
        this.directory = directory;
    }

    public Map<String, String> getActor() {
        return actor;
    }

    public void setActor(Map<String, String> actor) {
        this.actor = actor;
    }

    public String getActorPlay() {
        return actorPlay;
    }

    public void setActorPlay(String actorPlay) {
        this.actorPlay = actorPlay;
    }

    public String getOfficialUrl() {
        return officialUrl;
    }

    public void setOfficialUrl(String officialUrl) {
        this.officialUrl = officialUrl;
    }

    public Map<String, String> getLanguage() {
        return language;
    }

    public void setLanguage(Map<String, String> language) {
        this.language = language;
    }

    public String getSingleName() {
        return singleName;
    }

    public void setSingleName(String singleName) {
        this.singleName = singleName;
    }

    public Map<String, String> getStyle() {
        return style;
    }

    public void setStyle(Map<String, String> style) {
        this.style = style;
    }

    public Map<String, String> getInstructor() {
        return instructor;
    }

    public void setInstructor(Map<String, String> instructor) {
        this.instructor = instructor;
    }

    public Map<String, String> getCompere() {
        return compere;
    }

    public void setCompere(Map<String, String> compere) {
        this.compere = compere;
    }

    public Map<String, String> getGuest() {
        return guest;
    }

    public void setGuest(Map<String, String> guest) {
        this.guest = guest;
    }

    public String getSinger() {
        return singer;
    }

    public void setSinger(String singer) {
        this.singer = singer;
    }

    public Map<String, String> getSingerType() {
        return singerType;
    }

    public void setSingerType(Map<String, String> singerType) {
        this.singerType = singerType;
    }

    public String getMusicAuthors() {
        return musicAuthors;
    }

    public void setMusicAuthors(String musicAuthors) {
        this.musicAuthors = musicAuthors;
    }

    public String getMaker() {
        return maker;
    }

    public void setMaker(String maker) {
        this.maker = maker;
    }

    public String getRecordCompany() {
        return recordCompany;
    }

    public void setRecordCompany(String recordCompany) {
        this.recordCompany = recordCompany;
    }

    public String getIssueCompany() {
        return issueCompany;
    }

    public void setIssueCompany(String issueCompany) {
        this.issueCompany = issueCompany;
    }

    public Map<String, String> getTeam() {
        return team;
    }

    public void setTeam(Map<String, String> team) {
        this.team = team;
    }

    public Map<String, String> getPre() {
        return pre;
    }

    public void setPre(Map<String, String> pre) {
        this.pre = pre;
    }

    public String getQuality() {
        return quality;
    }

    public void setQuality(String quality) {
        this.quality = quality;
    }

    public Integer getIsHomemade() {
        return isHomemade;
    }

    public void setIsHomemade(Integer isHomemade) {
        this.isHomemade = isHomemade;
    }

    public Integer getDirection() {
        return direction;
    }

    public void setDirection(Integer direction) {
        this.direction = direction;
    }

    public int getLogoNum() {
        return logoNum;
    }

    public void setLogoNum(int logoNum) {
        this.logoNum = logoNum;
    }

    public Map<String, String> getPlayTv() {
        return playTv;
    }

    public void setPlayTv(Map<String, String> playTv) {
        this.playTv = playTv;
    }

    public Integer getPlayControlPlatformCntv() {
        return playControlPlatformCntv;
    }

    public void setPlayControlPlatformCntv(Integer playControlPlatformCntv) {
        this.playControlPlatformCntv = playControlPlatformCntv;
    }

    public Integer getPlayControlPlatformHuashu() {
        return playControlPlatformHuashu;
    }

    public void setPlayControlPlatformHuashu(Integer playControlPlatformHuashu) {
        this.playControlPlatformHuashu = playControlPlatformHuashu;
    }

    public Integer getPlayControlPlatformGuoguang() {
        return playControlPlatformGuoguang;
    }

    public void setPlayControlPlatformGuoguang(Integer playControlPlatformGuoguang) {
        this.playControlPlatformGuoguang = playControlPlatformGuoguang;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public Map<String, String> getFolkartType() {
        return folkartType;
    }

    public void setFolkartType(Map<String, String> folkartType) {
        this.folkartType = folkartType;
    }

    public String getVideoPic() {
        return videoPic;
    }

    public void setVideoPic(String videoPic) {
        this.videoPic = videoPic;
    }

    public Map<String, String> getFitAge() {
        return fitAge;
    }

    public void setFitAge(Map<String, String> fitAge) {
        this.fitAge = fitAge;
    }

    public Map<String, String> getContentRating() {
        return contentRating;
    }

    public void setContentRating(Map<String, String> contentRating) {
        this.contentRating = contentRating;
    }

    public Integer getCanSearch() {
        return canSearch;
    }

    public void setCanSearch(Integer canSearch) {
        this.canSearch = canSearch;
    }

    public Map<String, String> getRecommend() {
        return recommend;
    }

    public void setRecommend(Map<String, String> recommend) {
        this.recommend = recommend;
    }

    public String getActivityId() {
        return activityId;
    }

    public void setActivityId(String activityId) {
        this.activityId = activityId;
    }

    public Map<String, String> getDrmFlag() {
        return drmFlag;
    }

    public void setDrmFlag(Map<String, String> drmFlag) {
        this.drmFlag = drmFlag;
    }

    public Map<String, String> getSite() {
        return site;
    }

    public void setSite(Map<String, String> site) {
        this.site = site;
    }

    public int getIsFirstLook() {
        return isFirstLook;
    }

    public void setIsFirstLook(int isFirstLook) {
        this.isFirstLook = isFirstLook;
    }

    public Map<String, Object> getExtData() {
        return extData;
    }

    public void setExtData(Map<String, Object> extData) {
        this.extData = extData;
    }

    public String getCoopPlatform() {
        return coopPlatform;
    }

    public void setCoopPlatform(String coopPlatform) {
        this.coopPlatform = coopPlatform;
    }

    public String getLeId() {
        return leId;
    }

    public void setLeId(String leId) {
        this.leId = leId;
    }

    public String getAutoVideoPic() {
        return autoVideoPic;
    }

    public void setAutoVideoPic(String autoVideoPic) {
        this.autoVideoPic = autoVideoPic;
    }

    public Integer getIsPay() {
        return isPay;
    }

    public void setIsPay(Integer isPay) {
        this.isPay = isPay;
    }

    public Map<String, String> getPicAll() {
        return picAll;
    }

    public void setPicAll(Map<String, String> picAll) {
        this.picAll = picAll;
    }

    public Integer getDolbyFlag() {
        return dolbyFlag;
    }

    public void setDolbyFlag(Integer dolbyFlag) {
        this.dolbyFlag = dolbyFlag;
    }

    public String getVtypeFlag() {
        return vtypeFlag;
    }

    public void setVtypeFlag(String vtypeFlag) {
        this.vtypeFlag = vtypeFlag;
    }

    public Integer getVikiFlag() {
        return vikiFlag;
    }

    public void setVikiFlag(Integer vikiFlag) {
        this.vikiFlag = vikiFlag;
    }

    public Integer getMatchId() {
        return matchId;
    }

    public void setMatchId(Integer matchId) {
        this.matchId = matchId;
    }

    public String getCutoffPlatform() {
        return cutoffPlatform;
    }

    public void setCutoffPlatform(String cutoffPlatform) {
        this.cutoffPlatform = cutoffPlatform;
    }

    public String getCutoffTime() {
        return cutoffTime;
    }

    public void setCutoffTime(String cutoffTime) {
        this.cutoffTime = cutoffTime;
    }

    public Integer getIsPlayLock() {
        return isPlayLock;
    }

    public void setIsPlayLock(Integer isPlayLock) {
        this.isPlayLock = isPlayLock;
    }

    public Integer getIsBigPic() {
        return isBigPic;
    }

    public void setIsBigPic(Integer isBigPic) {
        this.isBigPic = isBigPic;
    }

    public Integer getIsDanmaku() {
        return isDanmaku;
    }

    public void setIsDanmaku(Integer isDanmaku) {
        this.isDanmaku = isDanmaku;
    }

    public Long getLiveId() {
        return liveId;
    }

    public void setLiveId(Long liveId) {
        this.liveId = liveId;
    }

    public Map<String, String> getPlatformFirstOnTime() {
        return platformFirstOnTime;
    }

    public void setPlatformFirstOnTime(Map<String, String> platformFirstOnTime) {
        this.platformFirstOnTime = platformFirstOnTime;
    }

    public void setPlatformFirstOnTime(String json) {
        this.platformFirstOnTime = JSON.parseObject(json, new TypeReference<Map<String, String>>() {
        });
    }

    public Map<String, String> getSubtitles() {
        return subtitles;
    }

    public void setSubtitles(Map<String, String> subtitles) {
        this.subtitles = subtitles;
    }

    public void setSubtitles(String json) {
        this.subtitles = JSON.parseObject(json, new TypeReference<Map<String, String>>() {
        });
    }

    public Map<String, String> getAudioTracks() {
        return audioTracks;
    }

    public void setAudioTracks(Map<String, String> audioTracks) {
        this.audioTracks = audioTracks;
    }

    public Map<String, String> getDefAudioTrack() {
        return defAudioTrack;
    }

    public void setDefAudioTrack(Map<String, String> defAudioTrack) {
        this.defAudioTrack = defAudioTrack;
    }

    public String getDefSubTrack() {
        return defSubTrack;
    }

    public void setDefSubTrack(String defSubTrack) {
        this.defSubTrack = defSubTrack;
    }

    public void setAudioTracks(String json) {
        this.audioTracks = JSON.parseObject(json, new TypeReference<Map<String, String>>() {
        });
    }

    public Map<String, String> getPlatformPreVid() {
        return platformPreVid;
    }

    public void setPlatformPreVid(Map<String, String> platformPreVid) {
        this.platformPreVid = platformPreVid;
    }

    public void setPlatformPreVid(String json) {
        this.platformPreVid = JSON.parseObject(json, new TypeReference<Map<String, String>>() {
        });
    }

    public Map<String, String> getPlatformNextVid() {
        return platformNextVid;
    }

    public void setPlatformNextVid(Map<String, String> platformNextVid) {
        this.platformNextVid = platformNextVid;
    }

    public void setPlatformNextVid(String json) {
        this.platformNextVid = JSON.parseObject(json, new TypeReference<Map<String, String>>() {
        });
    }

    public String getControlAreas() {
        return null;
    }

    public Map<String, String> getDisableType() {
        return null;
    }

    public Map<String, String> getLetvMakeStyle() {
        return null;
    }

    public Map<String, String> getLetvProduceStyle() {
        return null;
    }

    public String getMusicCompose() {
        return null;
    }

    public String getAudioInfo() {
        return audioInfo;
    }

    public void setAudioInfo(String audioInfo) {
        this.audioInfo = audioInfo;
    }

    public String getCopyrightCode() {
        return copyrightCode;
    }

    public void setCopyrightCode(String copyrightCode) {
        this.copyrightCode = copyrightCode;
    }

    public String getRelPositiveAlbumIds() {
        if (relPositiveAlbumIds == null) {
            return MapUtils.getString(extData, "rel_positive_album_ids");
        }
        return relPositiveAlbumIds;
    }

    public void setRelPositiveAlbumIds(String relPositiveAlbumIds) {
        this.relPositiveAlbumIds = relPositiveAlbumIds;
    }

    public String getShareCopy() {
        return shareCopy;
    }

    public void setShareCopy(String shareCopy) {
        this.shareCopy = shareCopy;
    }

    public Integer getIsVipDownload() {
        return isVipDownload;
    }

    public void setIsVipDownload(Integer isVipDownload) {
        this.isVipDownload = isVipDownload;
    }

}
