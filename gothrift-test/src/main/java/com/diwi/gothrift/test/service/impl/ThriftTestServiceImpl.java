package com.diwi.gothrift.test.service.impl;

import java.util.concurrent.atomic.AtomicInteger;

import org.apache.thrift.TException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.diwi.gothrift.core.annotation.GoService;
import com.diwi.gothrift.core.exceptions.RpcException;
import com.diwi.gothrift.test.service.ThriftTestService;

/**
 * @author dingwei
 * @since 16/3/7.
 */
@GoService(name = "t1", processor = ThriftTestService.Processor.class, iface = ThriftTestService.Iface.class)
public class ThriftTestServiceImpl implements ThriftTestService.Iface {
    private static final Logger logger = LoggerFactory.getLogger(ThriftTestService.class);
    private AtomicInteger count = new AtomicInteger(1);

    @Override
    public String sayHello(String username) throws TException {
        logger.info("thriftTestService is called, username is {}", username);
        int c = count.getAndIncrement();
        logger.info("thriftTestService called by {} times", c);
        if ((c & 2) != 0) {
            throw new RpcException("test");
        }

        return "thriftTestService: hello " + username;
    }
}
