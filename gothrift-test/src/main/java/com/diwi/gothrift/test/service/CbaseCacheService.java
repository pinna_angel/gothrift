package com.diwi.gothrift.test.service;

import java.io.Serializable;
import java.util.*;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;

/**
 * @author dingwei
 * @since 15/10/27.
 */
@Service
public class CbaseCacheService {
    @Resource
    private MemcacheClient memcacheClient;

    public <T> T get(String key) {

        return memcacheClient.get(key);
    }

    public <T> T get(String key, Class<T> clazz) {
        return memcacheClient.get(key, clazz);
    }

    public <T> Map<String, T> get(Collection<String> keys) {
        return memcacheClient.get(keys);
    }

    public <T> Map<String, T> get(Collection<String> keys, Class<T> clazz) {
        Map<String, String> jsonMap = this.get(keys);
        if (MapUtils.isEmpty(jsonMap)) {
            return null;
        }

        Map<String, T> result = new HashMap<String, T>();
        for (Map.Entry<String, String> entry : jsonMap.entrySet()) {
            result.put(entry.getKey(), JSON.parseObject(entry.getValue(), clazz));
        }
        return result;
    }

    public <T> List<T> mget(Collection<String> keys) {
        Map<String, T> result = get(keys);
        return convert(keys, result);
    }

    public <T> List<T> mget(String prefix, Collection<? extends Serializable> keys) {
        if (CollectionUtils.isEmpty(keys) || StringUtils.isEmpty(prefix)) {
            return null;
        }
        List<String> cacheKeys = new ArrayList<String>(keys.size());
        for (Object key : keys) {
            cacheKeys.add(prefix + key.toString());
        }
        Map<String, T> result = get(cacheKeys);
        return convert(cacheKeys, result);
    }

    public <T> List<T> mget(String prefix, Collection<? extends Serializable> keys, Class<T> clazz) {
        List<String> list = this.mget(prefix, keys);

        if (CollectionUtils.isEmpty(list)) {
            return Collections.emptyList();
        }

        List<T> result = new ArrayList<T>(list.size());
        for (String json : list) {
            if (StringUtils.isNotEmpty(json)) {
                result.add(JSON.parseObject(json, clazz));
            }
        }
        return result;
    }

    /**
     * 把无序Map转换成和idList顺序一致的List结果
     *
     * @param idList
     * @param resultMap
     * @return
     */
    <T> List<T> convert(Collection<String> idList, Map<String, T> resultMap) {
        if (CollectionUtils.isEmpty(idList) || MapUtils.isEmpty(resultMap)) {
            return Collections.emptyList();
        }
        List<T> sortList = new ArrayList<T>(resultMap.size());
        for (String li : idList) {
            sortList.add(resultMap.get(li));
        }
        return sortList;
    }

    public boolean set(String key, Object object) {
        return memcacheClient.set(key, object);
    }

    public boolean set(String key, int time, Object object) {
        return memcacheClient.set(key, time, object);
    }

    public boolean delete(String key) {
        return memcacheClient.delete(key);
    }
}
