package com.diwi.gothrift.test.rpc;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.diwi.gothrift.test.utils.Constants;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.diwi.gothrift.core.common.URL;
import com.diwi.gothrift.core.config.RegistryConfig;
import com.diwi.gothrift.core.router.RandomLoadBalance;
import com.diwi.gothrift.core.router.RoundRobinLoadBalance;
import com.diwi.gothrift.core.zookeeper.CuratorZookeeperClient;

/**
 * @author dingwei
 * @since 16/6/2.
 */
public class ServerTestCase {

    @Test
    public void startServerWithDelayExport() throws IOException {
        /* 测试服务接口的延迟暴露, 通过观察日志实现 */
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:rpc/server.xml");
        System.in.read();
        context.close();
    }

    @Test
    public void startServerWithLoadBalance() throws IOException, InterruptedException {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:rpc/server2.xml");

        TimeUnit.SECONDS.sleep(3);

        RegistryConfig registryConfig = context.getBean(RegistryConfig.class);
        CuratorZookeeperClient zkClient = new CuratorZookeeperClient(
                new URL(registryConfig.getAddress(), StringUtils.defaultString(registryConfig.getGroup(), "gothrift")));

        /* 测试负载均衡和权重的设置 */
        List<String> children = zkClient.getChildren(Constants.helloWorldService1);
        Assert.assertTrue(CollectionUtils.isNotEmpty(children));
        URL child = new URL(children.get(0));
        Assert.assertEquals(child.getLoadBalance(), RandomLoadBalance.NAME);
        Assert.assertEquals(child.getWeight(), 2);

        children = zkClient.getChildren(Constants.helloWorldService2);
        Assert.assertTrue(CollectionUtils.isNotEmpty(children));
        child = new URL(children.get(0));
        Assert.assertEquals(child.getLoadBalance(), RoundRobinLoadBalance.NAME);
        Assert.assertEquals(child.getWeight(), 3);

        children = zkClient.getChildren(Constants.thriftTestService);
        Assert.assertTrue(CollectionUtils.isNotEmpty(children));
        child = new URL(children.get(0));
        Assert.assertEquals(child.getLoadBalance(), null);
        Assert.assertEquals(child.getWeight(), 3);

        System.in.read();
        context.close();
    }
}
