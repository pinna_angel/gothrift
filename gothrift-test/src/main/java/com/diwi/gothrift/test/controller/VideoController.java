package com.diwi.gothrift.test.controller;

import javax.annotation.Resource;

import com.diwi.gothrift.test.utils.ThriftUtils;
import org.perf4j.StopWatch;
import org.perf4j.slf4j.Slf4JStopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.diwi.gothrift.test.bean.VideoPO;
import com.diwi.gothrift.test.service.CbaseCacheService;
import com.diwi.gothrift.test.service.VideoApiService;

/**
 * @author dingwei
 * @since 16/6/23.
 */
@Controller
@RequestMapping("/video")
public class VideoController {
    private static final Logger logger = LoggerFactory.getLogger(VideoController.class);

    // @Resource
    // private VideoService.Iface videoTService;
    @Resource
    private CbaseCacheService cbaseCacheService;
    @Resource
    private VideoApiService videoApiService;

    // @RequestMapping("/{id}")
    // @ResponseBody
    // public Object get(@PathVariable("id") int id) {
    // try {
    // return videoTService.get(id);
    // } catch (TException e) {
    // logger.error("exception", e);
    // }
    // return 500;
    // }

    @RequestMapping("/direct/{id}")
    @ResponseBody
    public Object direct(@PathVariable("id") int id) {
        StopWatch stopWatch = new Slf4JStopWatch("Service direct get(int)");
        VideoPO videoPO = videoApiService.get(id);
        stopWatch.lap("Video.html.cbase");

        try {
            return ThriftUtils.convert(videoPO);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } finally {
            stopWatch.lap("Video.html.convert");
        }
        return null;
    }
}
