package com.diwi.gothrift.test.service.impl;

import com.diwi.gothrift.test.service.HelloWorldService;
import org.apache.thrift.TException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.diwi.gothrift.core.annotation.GoService;

/**
 * @author dingwei
 * @since 16/3/7.
 */
@GoService(name = "h1", processor = HelloWorldService.Processor.class, iface = HelloWorldService.Iface.class, registry = {
        "r3", "r4" })
public class HelloWorldServiceImpl implements HelloWorldService.Iface {
    private static final Logger logger = LoggerFactory.getLogger(HelloWorldService.class);

    @Override
    public String sayHello(String username) throws TException {
        logger.info("helloWorldService is called, username is {}", username);
        return "helloWorldService: hello " + username;
    }
}
