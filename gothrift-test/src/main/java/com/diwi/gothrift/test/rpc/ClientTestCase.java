package com.diwi.gothrift.test.rpc;

import static com.diwi.gothrift.core.utils.Constants.AND;

import java.lang.reflect.Proxy;
import java.util.List;
import java.util.Map;

import com.diwi.gothrift.test.service.HelloWorldService;
import org.apache.commons.collections.CollectionUtils;
import org.apache.thrift.TException;
import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cglib.proxy.Enhancer;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.diwi.gothrift.core.common.URL;
import com.diwi.gothrift.core.config.ReferenceBean;
import com.diwi.gothrift.core.router.RoutingService;
import com.diwi.gothrift.test.service.ThriftTestService;

/**
 * @author dingwei
 * @since 16/6/3.
 */
// @RunWith(SpringJUnit4ClassRunner.class)
// @ContextConfiguration(locations = "classpath:rpc/client.xml")
public class ClientTestCase {
    private static final Logger log = LoggerFactory.getLogger(ClientTestCase.class);

    @Test
    public void testCall() throws TException {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:rpc/client.xml",
                "classpath:pool.xml");

        HelloWorldService.Iface h1 = (HelloWorldService.Iface) context.getBean("h1");
        HelloWorldService.Iface h2 = (HelloWorldService.Iface) context.getBean("h2");
        ThriftTestService.Iface t = (ThriftTestService.Iface) context.getBean("t");

        log.info(h1.sayHello("Kobe Bryant"));
        log.info(h2.sayHello("Tracy McGrady"));
        log.info(t.sayHello("Allen Iverson"));

        Assert.assertEquals("helloWorldService: hello Kobe Bryant", h1.sayHello("Kobe Bryant"));
        Assert.assertEquals("helloWorldService2: hello Tracy McGrady", h2.sayHello("Tracy McGrady"));
        // Assert.assertEquals("thriftTestService: hello Allen Iverson", t.sayHello("Allen Iverson"));
    }

    @Test
    public void testLoadBalanceAndProxy() {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:rpc/client2.xml");

        /* Proxy.isProxyClass(java.lang.Class<?> cl)判断是否是使用jdk反射来做动态代理 */
        HelloWorldService.Iface h1 = (HelloWorldService.Iface) context.getBean("h1");
        Assert.assertTrue(Proxy.isProxyClass(h1.getClass()));
        Assert.assertTrue(!Enhancer.isEnhanced(h1.getClass()));

        /* Enhancer.isEnhanced(java.lang.Class type)判断是否是使用cglib做动态代理 */
        HelloWorldService.Iface h2 = (HelloWorldService.Iface) context.getBean("h2");
        Assert.assertTrue(!Proxy.isProxyClass(h2.getClass()));
        Assert.assertTrue(Enhancer.isEnhanced(h2.getClass()));

        ThriftTestService.Iface t = (ThriftTestService.Iface) context.getBean("t");
        Assert.assertTrue(Proxy.isProxyClass(t.getClass()));

        HelloWorldService.Iface h11 = (HelloWorldService.Iface) context.getBean("h11");
        HelloWorldService.Iface h22 = (HelloWorldService.Iface) context.getBean("h22");
        Assert.assertTrue(Proxy.isProxyClass(h11.getClass()));
        Assert.assertTrue(Proxy.isProxyClass(h22.getClass()));

        /*
         * 客户端根据服务端接口配置的权重, 设置缓存中相应服务地址的数量, 例如127.0.0.1:8080服务的DemoService, 其weight设置为3,
         * 则客户端缓存的该接口服务地址中应包含三条127.0.0.1:8080
         */
        Map<String, ReferenceBean> beans = context.getBeansOfType(ReferenceBean.class);
        for (ReferenceBean referenceBean : beans.values()) {
            String serviceKey = referenceBean.getInterface() + AND + referenceBean.getVersion();
            List<URL> urls = RoutingService.availableServices(serviceKey);
            Assert.assertTrue(CollectionUtils.isNotEmpty(urls));
            URL url = urls.get(0);
            Assert.assertTrue("weight and the count of available service doesnot match.",
                    urls.size() == url.getWeight());
        }
    }
}
