package com.diwi.gothrift.test.service;

import java.util.*;
import java.util.concurrent.TimeoutException;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.MapUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.alibaba.fastjson.JSON;

import net.rubyeye.xmemcached.MemcachedClient;
import net.rubyeye.xmemcached.MemcachedClientCallable;
import net.rubyeye.xmemcached.exception.MemcachedException;

/**
 * @author dingwei
 * @since 15/10/27.
 */
@Service
public class MemcacheClient {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    // 循环次数
    private static final int LOOP_TIME = 3;
    // 超时时间
    private static final int TIME_OUT = 1000;
    // 过期时间
    // private static final int EVICT_TIME = 60 * 60 * 24 * 7;
    private static final int EVICT_TIME = 0; // 永久生效

    @Resource
    private MemcachedClient memcachedClient;

    public boolean set(String key, int time, Object object) {
        boolean result = false;
        int loop = 0;
        while (!result && loop < LOOP_TIME) {
            try {
                loop++;
                result = memcachedClient.set(key, time, object, TIME_OUT);
                if (logger.isDebugEnabled())
                    logger.debug(" set " + key + " " + result);
            } catch (TimeoutException e) {
                logger.error(key + " store into cbase, receive TimeoutException, try :" + loop + " times", e);
            } catch (InterruptedException e) {
                logger.error(key + " store into cbase, receive InterruptedException, try :" + loop + " times", e);
            } catch (MemcachedException e) {
                logger.error(key + " store into cbase, receive MemcachedException, try :" + loop + " times", e);
            }
        }
        return result;
    }

    public boolean add(String key, int time, Object object) {
        boolean result = false;
        int loop = 0;
        while (!result && loop < LOOP_TIME) {
            try {
                loop++;
                result = memcachedClient.add(key, time, object, TIME_OUT);
                if (logger.isDebugEnabled())
                    logger.debug(" add " + key + " " + result);
            } catch (TimeoutException e) {
                logger.error(key + " store into cbase, receive TimeoutException, try :" + loop + " times", e);
            } catch (InterruptedException e) {
                logger.error(key + " store into cbase, receive InterruptedException, try :" + loop + " times", e);
            } catch (MemcachedException e) {
                logger.error(key + " store into cbase, receive MemcachedException, try :" + loop + " times", e);
            }
        }
        return result;
    }

    public boolean delete(String key) {
        boolean result = false;
        int loop = 0;
        while (!result && loop < LOOP_TIME) {
            try {
                loop++;
                result = memcachedClient.delete(key);
                if (logger.isDebugEnabled())
                    logger.debug(" delete " + key + " " + result);
            } catch (TimeoutException e) {
                logger.error(key + " delete cbase, receive TimeoutException, try :" + loop + " times", e);
            } catch (InterruptedException e) {
                logger.error(key + " delete cbase, receive InterruptedException, try :" + loop + " times", e);
            } catch (MemcachedException e) {
                logger.error(key + " delete cbase, receive MemcachedException, try :" + loop + " times", e);
            }
        }
        return result;
    }

    public <T> T get(String key) {
        T result = null;
        boolean retry = false;
        int loop = 0;
        do {
            try {
                loop++;
                result = memcachedClient.get(key, TIME_OUT);
                retry = false;
            } catch (TimeoutException e) {
                logger.error(key + " get from cbase, receive TimeoutException, try :" + loop + " times", e);
                retry = true;
            } catch (InterruptedException e) {
                logger.error(key + " get from cbase, receive InterruptedException, try :" + loop + " times", e);
                retry = true;
            } catch (MemcachedException e) {
                logger.error(key + " get from cbase, receive MemcachedException, try :" + loop + " times", e);
                retry = true;
            }
        } while (retry && loop < LOOP_TIME);

        return result;
    }

    public <T> T get(String key, Class<T> clazz) {
        return JSON.parseObject(this.<String> get(key), clazz);
    }

    public <T> Map<String, T> get(Collection<String> keys) {
        Map<String, T> result = null;
        int loop = 0;
        boolean retry = false;

        do {
            try {
                loop++;
                result = memcachedClient.get(keys, TIME_OUT);
                retry = false;
            } catch (TimeoutException e) {
                logger.error(subKeys(keys) + " get from cbase, receive TimeoutException, try :" + loop + " times", e);
                retry = true;
            } catch (InterruptedException e) {
                logger.error(subKeys(keys) + " get from cbase, receive InterruptedException, try :" + loop + " times",
                        e);
                retry = true;
            } catch (MemcachedException e) {
                logger.error(subKeys(keys) + " get from cbase, receive MemcachedException, try :" + loop + " times", e);
                retry = true;
            }
        } while (retry && loop < LOOP_TIME);

        return result;
    }

    private String subKeys(Collection<String> keys) {
        if (CollectionUtils.isEmpty(keys)) {
            return "";
        }
        return keys.size() + ", " + keys.iterator().next() + "...";
    }

    public boolean set(String key, Object object) {
        return set(key, EVICT_TIME, object);
    }

    public boolean add(String key, Object object) {
        return add(key, EVICT_TIME, object);
    }

    public <T> List<T> mget(Collection<String> keys) {
        Map<String, T> result = get(keys);
        return convert(keys, result);
    }

    /**
     * 把无序Map转换成和idList顺序一致的List结果
     *
     * @param idList
     * @param resultMap
     * @return
     */
    <T> List<T> convert(Collection<String> idList, Map<String, T> resultMap) {
        if (CollectionUtils.isEmpty(idList) || MapUtils.isEmpty(resultMap)) {
            return Collections.emptyList();
        }
        List<T> sortList = new ArrayList<T>(resultMap.size());
        for (String li : idList) {
            sortList.add(resultMap.get(li));
        }
        return sortList;
    }

    /**
     * 往命名空间里面写值 @param <T> @param ns @param key @param value @return void @throws
     */
    public <T> void setWithNs(String ns, final String key, final T value) {
        try {
            memcachedClient.withNamespace(ns, new MemcachedClientCallable<Void>() {
                public Void call(MemcachedClient client)
                        throws MemcachedException, InterruptedException, TimeoutException {
                    memcachedClient.set(key, EVICT_TIME, value);
                    return null;
                }
            });
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

    }

    /**
     * 获取命名空间里面值 @param <T> @param ns @param key @param value @return void @throws
     */
    @SuppressWarnings("unchecked")
    public <T> T getWithNs(String ns, final String key) {
        Object obj = null;
        try {
            obj = memcachedClient.withNamespace(ns, new MemcachedClientCallable<String>() {
                public String call(MemcachedClient client)
                        throws MemcachedException, InterruptedException, TimeoutException {
                    return memcachedClient.get(key);
                }
            });
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        if (obj == null) {
            return null;
        } else {
            return (T) obj;
        }
    }

    /**
     * 命名空间无效 @param <T> @param ns @param key @param value @return void @throws
     */
    public void invalidateNamespace(String ns) {
        try {
            memcachedClient.invalidateNamespace(ns);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }

    }
}
