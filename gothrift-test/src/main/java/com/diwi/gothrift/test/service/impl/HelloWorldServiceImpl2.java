package com.diwi.gothrift.test.service.impl;

import com.diwi.gothrift.test.service.HelloWorldService;
import org.apache.thrift.TException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.diwi.gothrift.core.annotation.GoService;

/**
 * @author dingwei
 * @since 16/3/7.
 */
@GoService(name = "h2", processor = HelloWorldService.Processor.class, iface = HelloWorldService.Iface.class, registry = {
        "r2", "r4" }, version = "2.0")
public class HelloWorldServiceImpl2 implements HelloWorldService.Iface {
    private static final Logger logger = LoggerFactory.getLogger(HelloWorldService.class);

    @Override
    public String sayHello(String username) throws TException {
        logger.info("helloWorldService2 is called, username is {}", username);
        return "helloWorldService2: hello " + username;
    }
}
