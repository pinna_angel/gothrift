package com.diwi.gothrift.test.service;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.diwi.gothrift.test.bean.VideoPO;

/**
 * @author dingwei
 * @since 16/6/22.
 */
@Service
public class VideoApiService {
    @Resource
    private CbaseCacheService cbaseCacheService;

    public VideoPO get(long vid) {
        return cbaseCacheService.get("NEW_VIDEO_" + vid, VideoPO.class);
    }
}
