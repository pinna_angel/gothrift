package com.diwi.gothrift.test.registry;

import static com.diwi.gothrift.core.utils.Constants.REGISTRY_SPLIT_PATTERN;
import static com.diwi.gothrift.test.utils.Constants.helloWorldService1;
import static com.diwi.gothrift.test.utils.Constants.helloWorldService2;
import static com.diwi.gothrift.test.utils.Constants.thriftTestService;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.diwi.gothrift.core.common.URL;
import com.diwi.gothrift.core.config.RegistryConfig;
import com.diwi.gothrift.core.zookeeper.CuratorZookeeperClient;

/**
 * 注册中心测试用例
 * 
 * @author dingwei
 * @since 16/6/1.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:registry/registryWithAnnotation.xml" })
// @ContextConfiguration(locations = { "classpath:registry/registryWithXml.xml" })
public class RegistryTestCase {
    private static final Logger log = LoggerFactory.getLogger(RegistryTestCase.class);

    @Resource
    private RegistryConfig r1;
    @Resource
    private RegistryConfig r2;
    @Resource
    private RegistryConfig r3;
    @Resource
    private RegistryConfig r4;
    @Resource
    private RegistryConfig r5;

    /**
     * load registryWithXml.xml
     */
    @Test
    public void testXmlConfig() throws InterruptedException, IOException {
        CuratorZookeeperClient zkClient1 = new CuratorZookeeperClient(
                new URL(r1.getAddress(), StringUtils.defaultString(r1.getGroup(), "gothrift")));
        CuratorZookeeperClient zkClient2 = new CuratorZookeeperClient(
                new URL(r2.getAddress(), StringUtils.defaultString(r2.getGroup(), "gothrift")));
        CuratorZookeeperClient zkClient3 = new CuratorZookeeperClient(
                new URL(r3.getAddress(), StringUtils.defaultString(r3.getGroup(), "gothrift")));
        CuratorZookeeperClient zkClient4 = new CuratorZookeeperClient(
                new URL(r4.getAddress(), StringUtils.defaultString(r4.getGroup(), "gothrift")));

        TimeUnit.SECONDS.sleep(5); // wait for service registering to be finished.

        log.info("1. {}, list is {}", helloWorldService1, zkClient1.getChildren(helloWorldService1));
        log.info("1. {}, list is {}", helloWorldService2, zkClient1.getChildren(helloWorldService2));
        log.info("1. {}, list is {}", thriftTestService, zkClient1.getChildren(thriftTestService));
        Assert.assertTrue(zkClient1.getChildren(helloWorldService1).get(0).contains("h1"));
        Assert.assertTrue(zkClient1.getChildren(helloWorldService2).get(0).contains("h2"));
        Assert.assertTrue(zkClient1.getChildren(thriftTestService).get(0).contains("t1"));

        log.info("2. {}, list is {}", helloWorldService1, zkClient2.getChildren(helloWorldService1));
        log.info("2. {}, list is {}", helloWorldService2, zkClient2.getChildren(helloWorldService2));
        log.info("2. {}, list is {}", thriftTestService, zkClient2.getChildren(thriftTestService));
        Assert.assertTrue(zkClient2.getChildren(helloWorldService1).get(0).contains("h1"));
        Assert.assertTrue(CollectionUtils.isEmpty(zkClient2.getChildren(helloWorldService2)));
        Assert.assertTrue(zkClient2.getChildren(thriftTestService).get(0).contains("t1"));

        log.info("3. {}, list is {}", helloWorldService1, zkClient3.getChildren(helloWorldService1));
        log.info("3. {}, list is {}", helloWorldService2, zkClient3.getChildren(helloWorldService2));
        log.info("3. {}, list is {}", thriftTestService, zkClient3.getChildren(thriftTestService));
        Assert.assertTrue(CollectionUtils.isEmpty(zkClient3.getChildren(helloWorldService1)));
        Assert.assertTrue(zkClient3.getChildren(helloWorldService2).get(0).contains("h2"));
        Assert.assertTrue(zkClient3.getChildren(thriftTestService).get(0).contains("t1"));

        log.info("4. {}, list is {}", helloWorldService1, zkClient4.getChildren(helloWorldService1));
        log.info("4. {}, list is {}", helloWorldService2, zkClient4.getChildren(helloWorldService2));
        log.info("4. {}, list is {}", thriftTestService, zkClient4.getChildren(thriftTestService));
        Assert.assertTrue(CollectionUtils.isEmpty(zkClient4.getChildren(helloWorldService1)));
        Assert.assertTrue(CollectionUtils.isEmpty(zkClient4.getChildren(helloWorldService2)));
        Assert.assertTrue(zkClient4.getChildren(thriftTestService).get(0).contains("t1"));

        testR5();
    }

    private void testR5() {
        String[] addrs = REGISTRY_SPLIT_PATTERN.split(r5.getAddress());
        for (String addr : addrs) {
            CuratorZookeeperClient zkClient5 = new CuratorZookeeperClient(
                    new URL(addr, StringUtils.defaultString(r5.getGroup(), "gothrift")));
            Assert.assertTrue(CollectionUtils.isEmpty(zkClient5.getChildren(helloWorldService1)));
            Assert.assertTrue(CollectionUtils.isEmpty(zkClient5.getChildren(helloWorldService2)));
            Assert.assertTrue(zkClient5.getChildren(thriftTestService).get(0).contains("t1"));
        }
    }

    /**
     * load registryWithAnnotation.xml
     */
    @Test
    public void testAnnotationConfig() throws InterruptedException {
        CuratorZookeeperClient zkClient1 = new CuratorZookeeperClient(
                new URL(r1.getAddress(), StringUtils.defaultString(r1.getGroup(), "gothrift")));
        CuratorZookeeperClient zkClient2 = new CuratorZookeeperClient(
                new URL(r2.getAddress(), StringUtils.defaultString(r2.getGroup(), "gothrift")));
        CuratorZookeeperClient zkClient3 = new CuratorZookeeperClient(
                new URL(r3.getAddress(), StringUtils.defaultString(r3.getGroup(), "gothrift")));
        CuratorZookeeperClient zkClient4 = new CuratorZookeeperClient(
                new URL(r4.getAddress(), StringUtils.defaultString(r4.getGroup(), "gothrift")));

        TimeUnit.SECONDS.sleep(5); // wait for service registering to be finished.

        log.info("1. {}, list is {}", helloWorldService1, zkClient1.getChildren(helloWorldService1));
        log.info("1. {}, list is {}", helloWorldService2, zkClient1.getChildren(helloWorldService2));
        log.info("1. {}, list is {}", thriftTestService, zkClient1.getChildren(thriftTestService));
        Assert.assertTrue(CollectionUtils.isEmpty(zkClient1.getChildren(helloWorldService1)));
        Assert.assertTrue(CollectionUtils.isEmpty(zkClient1.getChildren(helloWorldService2)));
        Assert.assertTrue(zkClient1.getChildren(thriftTestService).get(0).contains("t1"));

        log.info("2. {}, list is {}", helloWorldService1, zkClient2.getChildren(helloWorldService1));
        log.info("2. {}, list is {}", helloWorldService2, zkClient2.getChildren(helloWorldService2));
        log.info("2. {}, list is {}", thriftTestService, zkClient2.getChildren(thriftTestService));
        Assert.assertTrue(CollectionUtils.isEmpty(zkClient2.getChildren(helloWorldService1)));
        Assert.assertTrue(zkClient2.getChildren(helloWorldService2).get(0).contains("h2"));
        Assert.assertTrue(zkClient2.getChildren(thriftTestService).get(0).contains("t1"));

        log.info("3. {}, list is {}", helloWorldService1, zkClient3.getChildren(helloWorldService1));
        log.info("3. {}, list is {}", helloWorldService2, zkClient3.getChildren(helloWorldService2));
        log.info("3. {}, list is {}", thriftTestService, zkClient3.getChildren(thriftTestService));
        Assert.assertTrue(zkClient3.getChildren(helloWorldService1).get(0).contains("h1"));
        Assert.assertTrue(CollectionUtils.isEmpty(zkClient3.getChildren(helloWorldService2)));
        Assert.assertTrue(zkClient3.getChildren(thriftTestService).get(0).contains("t1"));

        log.info("4. {}, list is {}", helloWorldService1, zkClient4.getChildren(helloWorldService1));
        log.info("4. {}, list is {}", helloWorldService2, zkClient4.getChildren(helloWorldService2));
        log.info("4. {}, list is {}", thriftTestService, zkClient4.getChildren(thriftTestService));
        Assert.assertTrue(zkClient4.getChildren(helloWorldService1).get(0).contains("h1"));
        Assert.assertTrue(zkClient4.getChildren(helloWorldService2).get(0).contains("h2"));
        Assert.assertTrue(zkClient4.getChildren(thriftTestService).get(0).contains("t1"));

        testR5();
    }
}
