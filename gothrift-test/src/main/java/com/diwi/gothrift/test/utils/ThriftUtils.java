package com.diwi.gothrift.test.utils;

import com.diwi.gothrift.test.thrift.VideoPO;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author dingwei
 * @since 16/6/22.
 */
public class ThriftUtils {
    public static <T> void idl(T t) throws IOException {
        Class<?> clazz = t.getClass();
        String className = clazz.getSimpleName();

        BufferedWriter writer = new BufferedWriter(new FileWriter("/Users/diwi/Documents/Letv/test.idl", true));
        writer.write("struct " + className + "{");
        writer.newLine();

        int i = 1;
        for (Field field : clazz.getDeclaredFields()) {
            if (Modifier.isStatic(field.getModifiers())) {
                continue;
            }
            String name = field.getName();
            Class<?> type = field.getType();

            writer.write("     " + (i++) + ": required " + type.getSimpleName().toLowerCase() + " " + name + ",");
            writer.newLine();
        }

        writer.write("}");
        writer.flush();
    }

    public static VideoPO convert(com.diwi.gothrift.test.bean.VideoPO videoPO)
            throws IllegalAccessException {
        VideoPO tVideo = new VideoPO();

        Class<VideoPO> tClazz = VideoPO.class;
        for (Field field : videoPO.getClass().getDeclaredFields()) {
            if (Modifier.isStatic(field.getModifiers())) {
                continue;
            }

            field.setAccessible(true);
            Object value = field.get(videoPO);
            Field tField = null;
            try {
                tField = tClazz.getDeclaredField(field.getName());
            } catch (NoSuchFieldException e) {
                continue;
            }
            tField.setAccessible(true);
            if (tField.getType().isPrimitive() && value == null) {
                value = 0;
            }
            if (Date.class.isAssignableFrom(field.getType())) {
                value = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(value);
            }
            tField.set(tVideo, value);
        }
        tVideo.setAliasStr("");
        return tVideo;
    }

    public static void main(String[] args) throws IOException {
        idl(new com.diwi.gothrift.test.bean.VideoPO());
    }
}
