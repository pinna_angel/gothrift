package com.diwi.gothrift.test.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.apache.thrift.TException;
import org.perf4j.StopWatch;
import org.perf4j.slf4j.Slf4JStopWatch;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.diwi.gothrift.test.service.VideoApiService;
import com.diwi.gothrift.test.service.VideoService;
import com.diwi.gothrift.test.thrift.VideoPO;
import com.diwi.gothrift.test.utils.ThriftUtils;

/**
 * @author dingwei
 * @since 16/6/23.
 */
@Service
public class VideoServiceImpl implements VideoService.Iface {
    private final static Logger logger = LoggerFactory.getLogger(VideoServiceImpl.class);

    @Resource
    private VideoApiService videoApiService;

    @Override
    public VideoPO get(int id) throws TException {
        StopWatch stopWatch = new Slf4JStopWatch("Service get(int)");
        com.diwi.gothrift.test.bean.VideoPO videoPO = videoApiService.get(id);
        stopWatch.lap("Video.cbase");
        try {
            return ThriftUtils.convert(videoPO);
        } catch (IllegalAccessException e) {
            logger.error("IllegalAccessException", e);
        } finally {
            stopWatch.lap("Video.convert");
        }
        return null;
    }

    @Override
    public List<VideoPO> gets(int id) throws TException {
        StopWatch stopWatch = new Slf4JStopWatch("Service gets(int)");

        List<VideoPO> list = Lists.newArrayList();
        com.diwi.gothrift.test.bean.VideoPO videoPO = videoApiService.get(id);
        stopWatch.lap("Videos.cbase");

        try {
            for (int i = 0; i < 10; i++) {
                list.add(ThriftUtils.convert(videoPO));
            }
        } catch (IllegalAccessException e) {
            logger.error("IllegalAccessException", e);
        }
        stopWatch.lap("Videos.convert");
        return list;
    }

}
