package com.diwi.gothrift.test;

import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import com.diwi.gothrift.test.service.HelloWorldService;
import com.diwi.gothrift.test.service.ThriftTestService;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.protocol.TMultiplexedProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransport;
import org.apache.thrift.transport.TTransportException;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * @author dingwei
 * @since 16/4/1.
 */
// @RunWith(SpringJUnit4ClassRunner.class)
// @ContextConfiguration(locations = { "classpath:service.xml", "classpath:client.xml" })
public class TestCase {
    private static final Logger logger = LoggerFactory.getLogger(TestCase.class);

    @Test
    public void testServer() throws IOException {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:service.xml");
        System.in.read();
        applicationContext.close();
    }

    @Test
    public void testServer1() throws IOException {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext(
                "classpath:service1.xml");
        System.in.read();
        applicationContext.close();
    }

    @Test
    public void testServer2() throws IOException {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext(
                "classpath:service2.xml");
        System.in.read();
        applicationContext.close();
    }

    @Test
    public void testServerWithXml() throws IOException {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:thrift.xml");
        System.in.read();
        context.close();
    }

    @Test
    public void testClient1() throws TException, IOException, InterruptedException {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:client.xml",
                "classpath:pool.xml");
        HelloWorldService.Iface helloClient = (HelloWorldService.Iface) applicationContext.getBean("helloWorldService");
        HelloWorldService.Iface helloClient1_1 = (HelloWorldService.Iface) applicationContext
                .getBean("helloWorldService1_1");
        ThriftTestService.Iface thriftTestService = (ThriftTestService.Iface) applicationContext
                .getBean("thriftTestService");

        // call(helloClient);
        asyncCall(helloClient, helloClient1_1, thriftTestService);
        System.in.read();
        applicationContext.close();
    }

    private void asyncCall(HelloWorldService.Iface helloClient, HelloWorldService.Iface helloClient2,
            ThriftTestService.Iface thriftTestService) throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(20);
        for (int i = 0; i < 100; i++) {
            Runnable runnable = new TestRunnable("go thrift " + i, helloClient, helloClient2, thriftTestService);
            executorService.submit(runnable);
        }
        executorService.awaitTermination(5, TimeUnit.SECONDS);
    }

    private void call(HelloWorldService.Iface helloClient) throws TException {
        for (int i = 0; i < 10; i++) {
            logger.info(helloClient.sayHello("go thrift " + i));
        }
    }

    private class TestRunnable implements Runnable {
        private String name;
        private HelloWorldService.Iface helloWorldService;
        private HelloWorldService.Iface helloWorldService2;
        private ThriftTestService.Iface thriftTestService;

        TestRunnable(String name, HelloWorldService.Iface helloWorldService, HelloWorldService.Iface helloWorldService2,
                ThriftTestService.Iface thriftTestService) {
            this.name = name;
            this.helloWorldService = helloWorldService;
            this.helloWorldService2 = helloWorldService2;
            this.thriftTestService = thriftTestService;
        }

        @Override
        public void run() {
            try {
                // logger.info(helloWorldService.sayHello(name));
                // logger.info(helloWorldService2.sayHello(name));
                logger.info(thriftTestService.sayHello(name));
            } catch (Exception e) {
                logger.error("test service invoke failed.", e);
            }
        }
    }

    @Test
    public void testClient2() throws IOException, TException {
        TTransport transport = null;
        TMultiplexedProtocol helloProtocol = null;
        try {
            transport = new TFramedTransport(new TSocket("10.58.105.179", 40880, 0));
            // 协议要和服务端一致
            TProtocol protocol = new TCompactProtocol(transport);
            helloProtocol = new TMultiplexedProtocol(protocol, "h1_1");
            // TProtocol protocol = new TBinaryProtocol(transport);
            HelloWorldService.Client client = new HelloWorldService.Client(helloProtocol);
            transport.open();
            long start = System.currentTimeMillis();
            String result = client.sayHello("xxxxxx");
            System.out.println("Thrift client result =: " + result + " " + (System.currentTimeMillis() - start));
        } catch (TTransportException e) {
            e.printStackTrace();
        } catch (TException e) {
            e.printStackTrace();
        } finally {
            ThriftTestService.Client client = new ThriftTestService.Client(helloProtocol);
            System.out.println("------" + client.sayHello("123123123123"));

            if (null != transport) {
                transport.close();
            }
        }
    }
}
