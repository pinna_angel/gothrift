package com.diwi.gothrift.test;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.diwi.gothrift.core.config.ApplicationConfig;
import com.diwi.gothrift.core.config.RegistryConfig;

/**
 * @author dingwei
 * @since 16/3/22.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath*:thrift.xml")
public class NamespaceTestCase implements ApplicationContextAware {
    private ApplicationContext applicationContext;

    @Resource
    private RegistryConfig registryConfig;

    @Test
    public void test() {
        ApplicationConfig applicationConfig = applicationContext.getBean(ApplicationConfig.class);
        System.out.println(applicationConfig);

        System.out.println(registryConfig);
    }

    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
