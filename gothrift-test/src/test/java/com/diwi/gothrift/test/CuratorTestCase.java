package com.diwi.gothrift.test;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import com.diwi.gothrift.core.common.URL;
import com.diwi.gothrift.core.zookeeper.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;

/**
 * @author dingwei
 * @since 16/3/30.
 */
public class CuratorTestCase {
    private static final Logger logger = LoggerFactory.getLogger(CuratorTestCase.class);

    private ZookeeperClient zookeeperClient;

    @Before
    public void init() {
        URL url = new URL("127.0.0.1:2181");
        url.setGroup("gothrift");
        zookeeperClient = new CuratorZookeeperClient(url);
    }

    @After
    public void destroy() {
        zookeeperClient.close();
    }

    @Test
    public void testCreate() throws IOException {
        zookeeperClient.create("/t1", true);
        System.in.read();
    }

    @Test
    public void testDelete() {
        zookeeperClient.delete("/gothrift");
    }

    @Test
    public void testState() throws IOException {
        zookeeperClient.addStateListener(new StateListener() {
            @Override
            public void stateChanged(int connected) {
                logger.info("====={}", connected);
            }
        });
        System.in.read();
    }

    @Test
    public void test() throws IOException {
        zookeeperClient.addChildListener("/", new NodeListener() {
            @Override
            public void nodeChanged(NodeEvent event) {
                logger.info("event = {}", JSON.toJSON(event));
                try {
                    TimeUnit.SECONDS.sleep(3);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                zookeeperClient.create(event.getPath(), true);
            }
        });
        System.in.read();
    }
}
