package com.diwi.gothrift.test;

import java.util.concurrent.CountDownLatch;

import javax.annotation.Resource;

import com.diwi.gothrift.test.service.HelloWorldService;
import org.apache.thrift.TException;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.protocol.TMultiplexedProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransportException;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.diwi.gothrift.core.pool.ThriftConnectionPool;

/**
 * @author dingwei
 * @since 16/4/7.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:pool.xml")
public class PoolTestCase {
    @Resource
    private ThriftConnectionPool pool;

    @Test
    public void testPool() throws TException, InterruptedException {
        System.out.println(pool);

        final String url = "127.0.0.1:29090";
        final CountDownLatch countDownLatch = new CountDownLatch(50);
        test(url, countDownLatch);
        countDownLatch.await();

        final CountDownLatch countDownLatch2 = new CountDownLatch(50);
        test("127.0.0.1:29091", countDownLatch2);
        countDownLatch2.await();
    }

    private void test(final String url, final CountDownLatch countDownLatch) {
        for (int i = 0; i < 50; i++) {
            new Thread() {
                @Override
                public void run() {
                    TSocket tSocket = null;

                    try {
                        tSocket = pool.getConnection(url);
                        System.out.println(url + "      " + tSocket);
                        TFramedTransport transport = new TFramedTransport(tSocket);

                        // 协议要和服务端一致
                        TProtocol protocol = new TCompactProtocol(transport);
                        TMultiplexedProtocol helloProtocol = new TMultiplexedProtocol(protocol, "helloWorldService");
                        // TProtocol protocol = new TBinaryProtocol(transport);
                        HelloWorldService.Client client = new HelloWorldService.Client(helloProtocol);
                        if (!transport.isOpen()) {
                            transport.open();
                        }

                        String result = client.sayHello("xxxxxx");
                        System.out.println("Thrift client result =: " + result);
                    } catch (TTransportException e) {
                        e.printStackTrace();
                    } catch (TException e) {
                        e.printStackTrace();
                    } finally {
                        pool.returnConnection(url, tSocket);
                        countDownLatch.countDown();
                    }
                }
            }.start();
        }
    }

}
