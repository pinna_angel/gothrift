### GoThrift——thrift服务化组件
#### 优势：
1. 服务端服务的注册与发布，以及客户端对服务的自动发现。当服务端实例的数量发生变化时，不需要去手动修改配置文件。使用zookeeper作为注册中心，curator－recipes工具类来处理服务的注册／发现／订阅；
2. 客户端使用动态代理，使得客户端与服务端的RPC调用对客户端用户透明；
3. 客户端的连接池，提升性能；
4. 负载均衡策略：目前支持随机／轮询，后期支持加权轮询／最少连接策略；
5. 采用Spring的配置方式，透明化的接入应用，减少对应用的侵入，只需用Spring加载GoThrift的配置即可。

#### 使用方法：
###### 服务端：用Spring配置声明暴露服务

    <!-- 注册中心地址 Address of registry center -->
    <thrift:registry id="registryConfig" address="127.0.0.1:2181"/>

    <!-- 服务端监听端口 The Server listening port -->
    <thrift:protocol port="29090"/>

    <bean id="helloWorldService" class="com.diwi.gothrift.test.HelloWorldServiceImpl"/>

    <bean id="appService" class="com.diwi.gothrift.core.spring.ServicePublisher"/>
###### 服务端：在服务提供方实现接口

    @GoService(name = "helloWorldService", processor = HelloWorldService.Processor.class, iface = HelloWorldService.Iface.class)
    public class HelloWorldServiceImpl implements HelloWorldService.Iface {
        private static final Logger logger = LoggerFactory.getLogger(HelloWorldService.class);

        @Override
        public String sayHello(String username) throws TException {
            logger.info("called, username is {}", username);
            return "hello " + username;
        }
    }

###### 客户端：通过Spring配置引用远程服务
    <!-- 注册中心地址 Address of registry center -->
    <thrift:registry id="registryConfig" address="127.0.0.1:2181"/>

    <thrift:reference id="helloWorldService" interface="com.diwi.gothrift.test.HelloWorldService" loadbalance="roundrobin"/>

强烈建议：在GoThrift配置之前配置连接池，否则每次RPC调用都会新建socket实例进行通信：

    <bean id="pool" class="com.diwi.gothrift.core.pool.ThriftConnectionPool">
          <property name="maxTotal" value="20"/>
          <property name="maxTotalPerKey" value="10"/>
          <property name="maxIdlePerKey" value="10"/>
          <property name="testOnBorrow" value="true"/>
          <property name="testOnReturn" value="true"/>
          <property name="testWhileIdle" value="true"/>
          <property name="conTimeOut" value="2000"/>
          <property name="maxWait" value="5000"/>
    </bean>
###### 客户端：加载Spring配置文件并调用远程服务
    @Test
    public void testClient1() throws TException, IOException, InterruptedException {
        ClassPathXmlApplicationContext applicationContext = new ClassPathXmlApplicationContext("classpath:client.xml",
                "classpath:pool.xml");
        HelloWorldService.Iface helloClient = (HelloWorldService.Iface) applicationContext.getBean("helloWorldService");

        asyncCall(helloClient);

        applicationContext.close();
    }

#### 配置参考
1. 服务端配置
	* 配置文件方式\<thrfit:service/>		

		属性 | 是否必填 | 类型 | 缺省值 | 描述 |
		------ | ------ | -----| ----| ----|
		name|可选|String||服务名称，客户端调用时需使用
		interface|必填|String||服务的接口名（thrift根据idl文件生成的接口类<mark>DemoService$Iface</mark>）
		ref|必填|String||服务对象实现引用
		version|可选|String|1.0|服务版本，通常在接口不兼容时版本号才需要升级
		registry|可选|String|缺省向所有registry注册|将service注册到指定的注册中心，其值为\<thrift:registry>的id属性，多个以逗号分隔。
		loadbalance|可选|String||负载均衡，支持随机和轮询（加权）
		weight|可选|int|1|权重值
		delay | 可选 | int| 0 | 服务延迟暴露，单位为毫秒

	* 注解方式@GoService

		属性 | 是否必填 | 缺省值| 描述|
		----| ------| ----| ---|
		name|必填||服务名称|
		processor|必填||值需为thrift生成的服务类的静态内部类Processor
		iface|必填||值需为thrift生成的服务类的静态内部类Iface
		`注：version、loadbalance、weight、registry同配置文件`
	
2. 客户端配置\<thrift:reference/>

	属性| 是否必填|缺省值|描述|
	------ | ------ | -----| ----|
	id | 必填| | 服务引用Bean ID | 
	interface | 必填 | | 服务接口类（thrift根据idl文件生成的接口类DemoService）
	registry| 可选| 缺省从所有注册中心获取服务列表后合并结果 | 从指定注册中心获取服务列表，其值为\<thrift:registry>的id属性，<del>多个以逗号分隔</del>。
	proxy| 可选| jdk | 客户端调用的代理方式：jdk、cglib
	timeout| 可选 | 缺省为0，客户端会一直等直到服务端处理完毕 | 客户端进行远程调用的连接时间和数据读取时间，单位为毫秒
	`注：version、loadbalance同服务端配置`
	
3. 注册中心配置\<thrift:registry/>

	属性 | 是否必填 | 类型 | 缺省值 | 描述 |
	------ | ------ | -----| ----| ----|
	id|可选|String|| 注册中心id，服务端、客户端指定某注册中心时需与此id保持一致|
	address|必填|String||注册中心地址，ip:port格式，多个直接以逗号分隔|
	group|可选|String|缺省为gothrift|分组，服务地址注册到zk中指定的根节点下|
	
4. 服务提供者协议设置\<thrift:protocol/>

	属性 | 是否必填 | 类型 | 缺省值 | 描述 |
	------ | ------ | -----| ----| ----|
	host | 可选 | String| 自动查找本机IP| 服务主机名，为空则自动查找本机IP，<mark>建议不要配置，由框架自动获取本机IP</mark>|
	port | 可选 | int| 缺省为40880，若被占用则自动往后取| thrift接口的服务端口|