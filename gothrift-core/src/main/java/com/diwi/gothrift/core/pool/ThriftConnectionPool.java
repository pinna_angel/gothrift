package com.diwi.gothrift.core.pool;

import com.diwi.gothrift.core.exceptions.RpcException;
import org.apache.commons.pool2.impl.GenericKeyedObjectPool;
import org.apache.commons.pool2.impl.GenericKeyedObjectPoolConfig;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.apache.thrift.transport.TSocket;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;

/**
 * @author dingwei
 * @since 16/4/7.
 */
public class ThriftConnectionPool<T extends TSocket> implements InitializingBean, DisposableBean {
    private static final Logger logger = LoggerFactory.getLogger(ThriftConnectionPool.class);

    /**
     * The object pool that internally manages our connections.
     */
    protected volatile GenericKeyedObjectPool<String, TSocket> connectionPool = null;

    /**
     * The maximum number of active connections that can be allocated from this pool at the same time, or negative for
     * no limit.
     */
    protected int maxTotal = GenericKeyedObjectPoolConfig.DEFAULT_MAX_TOTAL;

    protected int maxTotalPerKey = GenericKeyedObjectPoolConfig.DEFAULT_MAX_TOTAL_PER_KEY;

    protected int maxIdlePerKey = GenericKeyedObjectPoolConfig.DEFAULT_MAX_IDLE_PER_KEY;

    /**
     * The minimum number of active connections that can remain idle in the pool, without extra ones being created, or 0
     * to create none.
     */
    protected int minIdlePerKey = GenericKeyedObjectPoolConfig.DEFAULT_MIN_IDLE_PER_KEY;

    /**
     * The maximum number of milliseconds that the pool will wait (when there are no available connections)
     */
    protected long maxWait = GenericKeyedObjectPoolConfig.DEFAULT_MAX_WAIT_MILLIS;

    protected boolean testOnBorrow = true;
    protected boolean testOnReturn = false;
    protected long timeBetweenEvictionRunsMillis = GenericObjectPoolConfig.DEFAULT_TIME_BETWEEN_EVICTION_RUNS_MILLIS;
    protected int numTestsPerEvictionRun = GenericObjectPoolConfig.DEFAULT_NUM_TESTS_PER_EVICTION_RUN;
    protected long minEvictableIdleTimeMillis = GenericObjectPoolConfig.DEFAULT_MIN_EVICTABLE_IDLE_TIME_MILLIS;
    protected boolean testWhileIdle = false;
    private int conTimeOut = 0;

    /**
     * Creates a connection pool for this datasource. This method only exists so subclasses can replace the
     * implementation class.
     */
    private void createConnectionPool() {
        // Create an object pool to contain our active connections
        GenericKeyedObjectPool<String, TSocket> pool = new GenericKeyedObjectPool<String, TSocket>(
                new PooledConnectionFactory(conTimeOut));
        pool.setMaxTotal(maxTotal);
        pool.setMaxTotalPerKey(maxTotalPerKey);
        pool.setMaxIdlePerKey(maxIdlePerKey);
        pool.setMinIdlePerKey(minIdlePerKey);
        pool.setMaxWaitMillis(maxWait);
        pool.setTestOnBorrow(testOnBorrow);
        pool.setTestOnReturn(testOnReturn);
        pool.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);
        pool.setNumTestsPerEvictionRun(numTestsPerEvictionRun);
        pool.setMinEvictableIdleTimeMillis(minEvictableIdleTimeMillis);
        pool.setTestWhileIdle(testWhileIdle);
        connectionPool = pool;
    }

    /**
     * Create (if necessary) and return a socket to the remote provider.
     * 
     * @param key representation for socket
     * @return socket
     */
    public TSocket getConnection(String key) {
        try {
            return connectionPool.borrowObject(key);
        } catch (Exception e) {
            logger.error("get connection failed, remote address is {}", key, e);
            throw new RpcException(RpcException.NETWORK_EXCEPTION, key, e);
        }
    }

    public void returnConnection(String key, TSocket obj) {
        try {
            connectionPool.returnObject(key, obj);
        } catch (Exception e) {
            logger.error("return connection failed, remote address is {}", key, e);
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        createConnectionPool();
    }

    @Override
    public void destroy() throws Exception {
        if (connectionPool != null) {
            connectionPool.close();
        }
    }

    public int getMaxTotal() {
        return maxTotal;
    }

    public void setMaxTotal(int maxTotal) {
        this.maxTotal = maxTotal;
    }

    public int getMaxTotalPerKey() {
        return maxTotalPerKey;
    }

    public void setMaxTotalPerKey(int maxTotalPerKey) {
        this.maxTotalPerKey = maxTotalPerKey;
    }

    public int getMaxIdlePerKey() {
        return maxIdlePerKey;
    }

    public void setMaxIdlePerKey(int maxIdlePerKey) {
        this.maxIdlePerKey = maxIdlePerKey;
    }

    public int getMinIdlePerKey() {
        return minIdlePerKey;
    }

    public void setMinIdlePerKey(int minIdlePerKey) {
        this.minIdlePerKey = minIdlePerKey;
    }

    public long getMaxWait() {
        return maxWait;
    }

    public void setMaxWait(long maxWait) {
        this.maxWait = maxWait;
    }

    public boolean isTestOnBorrow() {
        return testOnBorrow;
    }

    public void setTestOnBorrow(boolean testOnBorrow) {
        this.testOnBorrow = testOnBorrow;
    }

    public boolean isTestOnReturn() {
        return testOnReturn;
    }

    public void setTestOnReturn(boolean testOnReturn) {
        this.testOnReturn = testOnReturn;
    }

    public long getTimeBetweenEvictionRunsMillis() {
        return timeBetweenEvictionRunsMillis;
    }

    public void setTimeBetweenEvictionRunsMillis(long timeBetweenEvictionRunsMillis) {
        this.timeBetweenEvictionRunsMillis = timeBetweenEvictionRunsMillis;
    }

    public int getNumTestsPerEvictionRun() {
        return numTestsPerEvictionRun;
    }

    public void setNumTestsPerEvictionRun(int numTestsPerEvictionRun) {
        this.numTestsPerEvictionRun = numTestsPerEvictionRun;
    }

    public long getMinEvictableIdleTimeMillis() {
        return minEvictableIdleTimeMillis;
    }

    public void setMinEvictableIdleTimeMillis(long minEvictableIdleTimeMillis) {
        this.minEvictableIdleTimeMillis = minEvictableIdleTimeMillis;
    }

    public boolean isTestWhileIdle() {
        return testWhileIdle;
    }

    public void setTestWhileIdle(boolean testWhileIdle) {
        this.testWhileIdle = testWhileIdle;
    }

    public int getConTimeOut() {
        return conTimeOut;
    }

    public void setConTimeOut(int conTimeOut) {
        this.conTimeOut = conTimeOut;
    }

}
