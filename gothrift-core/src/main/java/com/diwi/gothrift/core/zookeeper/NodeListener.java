package com.diwi.gothrift.core.zookeeper;

public interface NodeListener {

    void nodeChanged(NodeEvent event);

}
