package com.diwi.gothrift.core.config;

import java.io.Serializable;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author dingwei
 * @since 16/3/21.
 */
public abstract class AbstractConfig implements Serializable {
    private static final long serialVersionUID = 8712932090791047320L;
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    protected String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
