package com.diwi.gothrift.core.config;

import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;

import com.diwi.gothrift.core.pool.ThriftConnectionPool;
import com.diwi.gothrift.core.proxy.CglibProxySource;
import com.diwi.gothrift.core.registry.CuratorZookeeperRegistry;
import com.diwi.gothrift.core.registry.Registry;
import com.diwi.gothrift.core.router.RandomLoadBalance;
import com.diwi.gothrift.core.zookeeper.NodeEvent;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.FactoryBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

import com.google.common.collect.Sets;
import com.diwi.gothrift.core.common.URL;
import com.diwi.gothrift.core.proxy.BaseProxySource;
import com.diwi.gothrift.core.proxy.JdkProxySource;
import com.diwi.gothrift.core.router.RoutingService;
import com.diwi.gothrift.core.utils.Constants;
import com.diwi.gothrift.core.zookeeper.NodeListener;

import static com.diwi.gothrift.core.utils.Constants.AND;
import static com.diwi.gothrift.core.utils.Constants.SERVICE_NODE_PATTERN;
import static com.diwi.gothrift.core.utils.Constants.SLASH;

/**
 * @author dingwei
 * @since 16/3/21.
 */
public class ReferenceBean extends AbstractConfig implements InitializingBean, FactoryBean, ApplicationContextAware {
    private static final long serialVersionUID = -2456245682523358675L;

    private String interfaceName;
    private int timeout;
    private String version;
    private String loadbalance;
    private String proxy;
    private RegistryConfig registry;

    private Object proxyClient;
    private Class<?> targetClass;

    private ThriftConnectionPool pool;

    private transient ApplicationContext applicationContext;

    public String getInterface() {
        return interfaceName;
    }

    public void setInterface(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public int getTimeout() {
        return timeout;
    }

    public void setTimeout(int timeout) {
        this.timeout = timeout;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getLoadbalance() {
        return loadbalance;
    }

    public void setLoadbalance(String loadbalance) {
        this.loadbalance = loadbalance;
    }

    public String getProxy() {
        return proxy;
    }

    public void setProxy(String proxy) {
        this.proxy = proxy;
    }

    public RegistryConfig getRegistry() {
        return registry;
    }

    public void setRegistry(RegistryConfig registry) {
        this.registry = registry;
    }

    public ThriftConnectionPool getPool() {
        return pool;
    }

    public void setPool(ThriftConnectionPool pool) {
        this.pool = pool;
    }

    private String getServiceKey() {
        return interfaceName + AND + version;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        if (registry == null) {
            Map<String, RegistryConfig> registryMap = applicationContext.getBeansOfType(RegistryConfig.class);
            for (RegistryConfig registryConfig : registryMap.values()) {
                if (StringUtils.isEmpty(registryConfig.getGroup())) {
                    registry = registryConfig;
                    break;
                }
            }

            if (registry == null) {
                RegistryConfig[] array = registryMap.values().toArray(new RegistryConfig[0]);
                registry = array.length == 0 ? null : array[0];
            }
        }

        if (registry == null) {
            logger.warn("registry has not been configured.");
            return;
        }

        String[] registries = Constants.REGISTRY_SPLIT_PATTERN.split(registry.getAddress());
        if (ArrayUtils.isEmpty(registries)) {
            logger.warn("registry address cannot be empty.");
            return;
        }

        subscribeService(registries);

        initConnectionPool();

        if (this.loadbalance == null) {
            List<URL> urls = RoutingService.availableServices(getServiceKey());
            this.loadbalance = urls.get(0).getLoadBalance();
            if (this.loadbalance == null) {
                this.loadbalance = RandomLoadBalance.NAME;
            }
        }

        final String serviceName = this.interfaceName;
        ClassLoader classLoader = this.getClass().getClassLoader();
        this.targetClass = Class.forName(serviceName + "$Iface", true, classLoader);

        BaseProxySource proxySource = Proxy.of(proxy) == Proxy.jdk
                ? new JdkProxySource(classLoader, new Class[] { targetClass }, this)
                : new CglibProxySource(targetClass, this);
        this.proxyClient = proxySource.createProxy();
    }

    /**
     * for thrift client's subscribing service, then cache to local memory
     *
     * @param registries registry addresses
     */
    private void subscribeService(String[] registries) {
        Set<String> servicePaths = Sets.newHashSet();
        final String serviceKey = getServiceKey();
        for (String addr : registries) {
            String group = this.registry.getGroup();
            Registry registry = CuratorZookeeperRegistry.getRegistry(new URL(addr, group));

            // do subscribe, and get the path's children
            List<String> serviceAddresses = registry.subscribe(SLASH + interfaceName + "$Iface" + SLASH + version,
                    new NodeListener() {
                        @Override
                        public void nodeChanged(NodeEvent event) {
                            String location = getServiceLocation(event.getPath());
                            if (StringUtils.isEmpty(location)) {
                                logger.warn("Invalid service found, address is {}", event.getPath());
                                return;
                            }

                            NodeEvent.Type type = event.getType();
                            switch (type) {
                            case CHILD_ADDED:
                                RoutingService.on(serviceKey, new URL(location));
                                logger.info("NEW service[{}] is found, address is {}, all providers are {}",
                                        interfaceName, location, RoutingService.availableServices(serviceKey));
                                break;

                            case CHILD_REMOVED:
                                RoutingService.off(serviceKey, new URL(location));
                                logger.info("Service[{}] has stopped serving, address is {}, all providers are {}",
                                        interfaceName, location, RoutingService.availableServices(serviceKey));
                                break;
                            }
                        }

                    });
            if (CollectionUtils.isEmpty(serviceAddresses)) {
                logger.warn("Failed to check the status of the service " + interfaceName
                        + ". No provider available for the service " + (group == null ? "" : group + "/")
                        + interfaceName + (version == null ? "" : ":" + version) + " from registry[" + addr + "]");
            }
            servicePaths.addAll(serviceAddresses);
        }

        // cache provider addresses to local memory
        Set<URL> locations = Sets.newHashSet();
        for (String path : servicePaths) {
            String location = getServiceLocation(path);
            if (StringUtils.isNotEmpty(location)) {
                URL url = new URL(location);
                locations.add(url);
            }
        }

        if (CollectionUtils.isEmpty(locations)) {
            throw new IllegalStateException("Failed to check the status of the service " + interfaceName
                    + ". No provider available for the service " + interfaceName
                    + (version == null ? "" : ":" + version) + " from registry[" + registry.getAddress() + "]");
        }

        // batch add, to avoid too much lock operations
        RoutingService.on(serviceKey, locations);
    }

    /**
     * thrift client connection pool initialization if configured
     */
    private void initConnectionPool() {
        if (pool == null) {
            ThriftConnectionPool connectionPool = null;
            try {
                connectionPool = applicationContext.getBean(ThriftConnectionPool.class);
            } catch (BeansException e) {
            }

            if (connectionPool == null) {
                logger.warn(
                        "Pool hasn't been configured, it will cause socket's creating, initializing and destroy once a RPC call is invoked");
            }
            pool = connectionPool;
        }
    }

    // get address from zookeeper node's path
    private String getServiceLocation(String path) {
        Matcher matcher = SERVICE_NODE_PATTERN.matcher(path);
        if (matcher.matches()) {
            String serviceName = matcher.group(1);
            String version = matcher.group(2);
            String address = matcher.group(3);
            logger.info("RPC service found, name is {}, version is {}, location is {}", serviceName, version, address);

            if (version.equalsIgnoreCase(this.version)) {
                return address;
            }
        }

        return null;
    }

    @Override
    public Object getObject() throws Exception {
        return proxyClient;
    }

    @Override
    public Class<?> getObjectType() {
        return targetClass;
    }

    @Override
    public boolean isSingleton() {
        return true;
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }

    enum Proxy {
        jdk, cglib;

        public static Proxy of(String type) {
            if (jdk.name().equalsIgnoreCase(type)) {
                return jdk;
            }

            if (cglib.name().equalsIgnoreCase(type)) {
                return cglib;
            }

            return jdk;
        }
    }
}
