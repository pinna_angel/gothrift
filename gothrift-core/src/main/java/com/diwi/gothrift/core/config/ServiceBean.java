package com.diwi.gothrift.core.config;

import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import com.diwi.gothrift.core.utils.Constants;
import com.diwi.gothrift.core.utils.NetUtils;
import com.diwi.gothrift.core.utils.ServerUtils;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.thrift.TMultiplexedProcessor;
import org.apache.thrift.TProcessor;
import org.springframework.beans.BeanUtils;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

/**
 * xml-based service registering and publishing
 * 
 * @author dingwei
 * @since 16/4/27.
 */
public class ServiceBean<T> extends AbstractServiceConfig {
    private static final long serialVersionUID = 968596871788778601L;

    private T ref;
    private String interfaceName;
    private Class<?> interfaceClass;
    private String version;
    private String loadbalance;
    private int weight;
    // 注册中心
    protected List<RegistryConfig> registries;

    private static ConcurrentMap<ServiceBean, Class> SERVICE_MAP = new ConcurrentHashMap<ServiceBean, Class>();

    private static AtomicInteger state = new AtomicInteger(0); // 0--unpublished, 1--published

    private static final CountDownLatch latch = new CountDownLatch(1);

    @Override
    public void init() {
        // service registering
        checkRef();
        SERVICE_MAP.put(this, getInterfaceClass());

        if (getRegistry() == null) {
            Map<String, RegistryConfig> registryConfigMap = CONTEXT.getBeansOfType(RegistryConfig.class);
            Preconditions.checkArgument(MapUtils.isNotEmpty(registryConfigMap),
                    "The <thrift:registry> configuration is needed.");
            setRegistries(Lists.newArrayList(registryConfigMap.values()));
        }

        new Thread(new Runnable() {
            @Override
            public void run() {
                long start = System.currentTimeMillis();
                if (delay != null && delay > 0) {
                    try {
                        TimeUnit.MILLISECONDS.sleep(delay);
                    } catch (InterruptedException e) {
                        logger.error("Delaying registering service is interrupted.");
                    }
                    logger.info("Service exporting delayed for {} seconds.", System.currentTimeMillis() - start);
                }

                try {
                    latch.await(); // to ensure that RPC service has started to serving
                    final String location = ServerUtils.getServiceLocation(protocolConfig);
                    doRegisterService(location);
                    logger.info("Service exporting takes {}ms, name is {}", System.currentTimeMillis() - start,
                            getId());
                } catch (InterruptedException e) {
                    logger.error("Waiting for registering service is interrupted.");
                }
            }
        }).start();
    }

    @Override
    protected boolean canPublish() {
        // to ensure publish() method is only invoked once.
        if (state.get() == 0) {
            synchronized (ServiceBean.class) {
                if (state.get() != 0) {
                    return false;
                }

                state.incrementAndGet();
                return true;
            }
        }

        return false;
        /* 可以简化成如下方式：只有一个线程执行cas会成功，保证只被发布一次 */
        // return state.compareAndSet(0, 1);
    }

    public T getRef() {
        return ref;
    }

    public void setRef(T ref) {
        this.ref = ref;
    }

    public String getInterface() {
        return interfaceName;
    }

    public void setInterface(String interfaceName) {
        this.interfaceName = interfaceName;
    }

    public Class<?> getInterfaceClass() {
        if (interfaceClass != null) {
            return interfaceClass;
        }

        try {
            if (interfaceName != null && interfaceName.length() > 0) {
                this.interfaceClass = Class.forName(interfaceName, true,
                        Thread.currentThread().getContextClassLoader());
            }
        } catch (ClassNotFoundException t) {
            throw new IllegalStateException(t.getMessage(), t);
        }
        return interfaceClass;
    }

    public void setInterfaceClass(Class<?> interfaceClass) {
        if (interfaceClass != null && !interfaceClass.isInterface()) {
            throw new IllegalStateException("The interface class " + interfaceClass + " is not a interface!");
        }
        this.interfaceClass = interfaceClass;
        setInterface(interfaceClass == null ? null : interfaceClass.getName());
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getLoadbalance() {
        return loadbalance;
    }

    public void setLoadbalance(String loadbalance) {
        this.loadbalance = loadbalance;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public RegistryConfig getRegistry() {
        return registries == null || registries.size() == 0 ? null : registries.get(0);
    }

    public void setRegistry(RegistryConfig registry) {
        List<RegistryConfig> registries = new ArrayList<RegistryConfig>(1);
        registries.add(registry);
        this.registries = registries;
    }

    public List<RegistryConfig> getRegistries() {
        return registries;
    }

    public void setRegistries(List<RegistryConfig> registries) {
        this.registries = registries;
    }

    private void checkRef() {
        // 检查引用不为空，并且引用必需实现接口
        if (ref == null) {
            throw new IllegalStateException("ref not allow null!");
        }
        if (!getInterfaceClass().isInstance(ref)) {
            throw new IllegalStateException(
                    "The class " + ref.getClass().getName() + " unimplemented interface " + interfaceClass + "!");
        }
    }

    private void doRegisterService(final String location) {
        StringBuilder buff = new StringBuilder(Constants.SLASH).append(this.getInterfaceClass().getName()).append(Constants.SLASH)
                .append(this.getVersion());

        String serviceRoot = buff.toString();

        buff.append(location).append("?name=").append(this.getId());

        // 负载均衡策略 load balance strategy
        if (StringUtils.isNotEmpty(this.getLoadbalance())) {
            buff.append("&loadbalance=").append(this.getLoadbalance());
        }

        // 权重 weight
        buff.append("&weight=").append(this.getWeight());

        String serviceNode = buff.toString();
        SERVICE_NODE.put(serviceNode, serviceRoot);

        registerAndSubscribe(this.getRegistries(), serviceNode, serviceRoot, location);
    }

    protected void publish() {
        if (MapUtils.isEmpty(SERVICE_MAP)) {
            logger.info("Thrift services have not been configured.");
            return;
        }
        logger.info("Initializing and exporting thrift service.");

        ProtocolConfig protocolConfig = CONTEXT.getBean(ProtocolConfig.class);
        if (protocolConfig == null) {
            protocolConfig = new ProtocolConfig();
            protocolConfig.setPort(NetUtils.getAvailablePort(Constants.DEFAULT_PORT));
        } else if (protocolConfig.getPort() == null) {
            protocolConfig.setPort(NetUtils.getAvailablePort(Constants.DEFAULT_PORT));
        }

        ServiceBean.protocolConfig = protocolConfig;
        doStartService(protocolConfig.getPort());

        latch.countDown();
    }

    protected TMultiplexedProcessor getProcessor() {
        TMultiplexedProcessor tMultiplexedProcessor = new TMultiplexedProcessor();
        Map<String, TProcessor> serviceMap = Maps.newHashMap();
        Map<ServiceBean, Class> serviceBeanMap = SERVICE_MAP;
        for (Map.Entry<ServiceBean, Class> entry : serviceBeanMap.entrySet()) {
            ServiceBean bean = entry.getKey();
            Class ifaceClass = entry.getValue();

            String processorClassName = ifaceClass.getName().replace("$Iface", "$Processor");

            Class<?> processorClass = null;
            try {
                processorClass = Class.forName(processorClassName, true,
                        Thread.currentThread().getContextClassLoader());

                Constructor<?> constructor = processorClass.getConstructor(ifaceClass);
                TProcessor processor = (TProcessor) BeanUtils.instantiateClass(constructor, bean.getRef());
                tMultiplexedProcessor.registerProcessor(bean.getId(), processor);
                serviceMap.put(bean.getId(), processor);
            } catch (ClassNotFoundException e) {
                logger.error("Processor for service {}, is not found.", ifaceClass.getName(), e);
            } catch (NoSuchMethodException e) {
                logger.error("Processor for service {}, constructor is not found.", ifaceClass.getName(), e);
            }

        }
        Preconditions.checkArgument(MapUtils.isNotEmpty(serviceMap), "thrift service configurations error.");
        return tMultiplexedProcessor;
    }

}
