package com.diwi.gothrift.core.registry;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.ReentrantLock;

import com.diwi.gothrift.core.zookeeper.CuratorZookeeperClient;
import org.apache.commons.lang3.StringUtils;
import org.apache.curator.framework.recipes.cache.ChildData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.Lists;
import com.diwi.gothrift.core.common.URL;
import com.diwi.gothrift.core.zookeeper.NodeListener;
import com.diwi.gothrift.core.zookeeper.StateListener;
import com.diwi.gothrift.core.zookeeper.ZookeeperClient;

/**
 * @author dingwei
 * @since 16/3/24.
 */
public class CuratorZookeeperRegistry extends AbstractRegistry {
    private static final Logger LOGGER = LoggerFactory.getLogger(CuratorZookeeperRegistry.class);
    private final static int DEFAULT_ZOOKEEPER_PORT = 2181;
    private final static String DEFAULT_ROOT = "gothrift";

    private ZookeeperClient zkClient;

    // 注册中心获取过程锁
    private static final ReentrantLock LOCK = new ReentrantLock();

    // 注册中心集合 Map<RegistryAddress, Registry>
    private static final Map<String, Registry> REGISTRIES = new ConcurrentHashMap<String, Registry>();

    private CuratorZookeeperRegistry(URL url) {
        if (url.getPort() <= 0) {
            url.setPort(DEFAULT_ZOOKEEPER_PORT);
        }

        if (StringUtils.isEmpty(url.getGroup())) {
            url.setGroup(DEFAULT_ROOT);
        }

        zkClient = new CuratorZookeeperClient(url);
        zkClient.addStateListener(new StateListener() {
            @Override
            public void stateChanged(int connected) {
                if (connected == StateListener.RECONNECTED) {
                    recover();
                }
            }
        });
    }

    public static Registry getRegistry(URL url) {
        String key = url.toString();
        // 锁定注册中心获取过程，保证注册中心单一实例
        LOCK.lock();
        try {
            Registry registry = REGISTRIES.get(key);
            if (registry != null) {
                return registry;
            }

            registry = new CuratorZookeeperRegistry(url);
            REGISTRIES.put(key, registry);
            return registry;
        } finally {
            // 释放锁
            LOCK.unlock();
        }
    }

    @Override
    protected void doRegister(String path) {
        zkClient.create(path, true);
    }

    @Override
    protected void doUnregister(String path) {
        zkClient.delete(path);
    }

    @Override
    protected List<String> doSubscribe(String path, NodeListener nodeListener) {
        List<ChildData> children = zkClient.addChildListener(path, nodeListener);

        List<String> list = Lists.newArrayList();
        if (children != null) {
            for (ChildData child : children) {
                list.add(child.getPath());
            }
        }
        return list;
    }

    @Override
    protected void doUnsubscribe(String path) {
        zkClient.removeChildListener(path);
    }
}
