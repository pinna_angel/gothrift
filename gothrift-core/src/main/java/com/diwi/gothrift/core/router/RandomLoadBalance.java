package com.diwi.gothrift.core.router;

import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

/**
 * 随机 Random
 * 
 * @author dingwei
 * @since 16/4/12.
 */
public class RandomLoadBalance extends AbstractLoadBalance {
    private static final ThreadLocalRandom random = ThreadLocalRandom.current();

    public static final String NAME = "random";

    private RandomLoadBalance() {
    }

    private static class Builder {
        private static final RandomLoadBalance instance = new RandomLoadBalance();

        static LoadBalance build() {
            return instance;
        }
    }

    public static LoadBalance getInstance() {
        return Builder.build();
    }

    @Override
    public <T> T doSelect(List<T> list) {
        int index = random.nextInt(list.size());
        return list.get(index);
    }
}
