package com.diwi.gothrift.core.utils;

import java.util.regex.Pattern;

import com.google.common.base.Splitter;

/**
 * @author dingwei
 * @since 16/4/21.
 */
public class Constants {

    public static final Pattern REGISTRY_SPLIT_PATTERN = Pattern.compile("\\s*[|;,]+\\s*");

    public static final Pattern SERVICE_NODE_PATTERN = Pattern.compile("/([^/]*)/([^/]*)/([^/]*)");

    public static final String SLASH = "/";

    public static final String AND = "&";

    public static final String EQUALS = "=";

    public static final int DEFAULT_PORT = 40880;

    public static final Splitter AND_SPLITTER = Splitter.on(AND).omitEmptyStrings().trimResults();

}
