package com.diwi.gothrift.core.proxy;

import java.lang.reflect.Method;

import org.springframework.cglib.proxy.Enhancer;
import org.springframework.cglib.proxy.MethodInterceptor;
import org.springframework.cglib.proxy.MethodProxy;

import com.diwi.gothrift.core.config.ReferenceBean;

/**
 * @author dingwei
 * @since 16/5/26.
 */
public class CglibProxySource extends BaseProxySource implements MethodInterceptor {
    private final Class<?> superclass;

    public CglibProxySource(Class<?> superclass, ReferenceBean referenceBean) {
        super(referenceBean);
        this.superclass = superclass;
    }

    @Override
    public Object createProxy() {
        Enhancer enhancer = new Enhancer();
        enhancer.setSuperclass(superclass);
        enhancer.setCallback(this);
        return enhancer.create();
    }

    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        return handle(method, objects);
    }
}
