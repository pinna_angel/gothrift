package com.diwi.gothrift.core.annotation;

import static java.lang.annotation.ElementType.TYPE;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import com.diwi.gothrift.core.router.RandomLoadBalance;

/**
 * @author dingwei
 * @since 16/3/31.
 */
@Documented
@Target({ TYPE })
@Retention(RUNTIME)
public @interface GoService {
    /**
     * @return the name of thrift service
     */
    String name();

    /**
     * @return the processor class of thrift service
     */
    Class<?> processor();

    /**
     * @return the iface class of thrift service
     */
    Class<?> iface();

    String version() default "1.0";

    String loadbalance() default RandomLoadBalance.NAME;

    int weight() default 1;

    /**
     * values MUST be selected from the ids of <u>thrift:registry</u> elements
     */
    String[] registry() default {};
}
