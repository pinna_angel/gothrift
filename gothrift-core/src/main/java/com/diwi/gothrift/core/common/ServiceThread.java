package com.diwi.gothrift.core.common;

import org.apache.thrift.TMultiplexedProcessor;
import org.apache.thrift.TProcessor;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.server.TServer;
import org.apache.thrift.server.TThreadedSelectorServer;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TNonblockingServerSocket;
import org.apache.thrift.transport.TTransportException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author dingwei
 * @since 16/4/29.
 */
public class ServiceThread extends Thread {
    protected static final Logger logger = LoggerFactory.getLogger(ServiceThread.class);
    private static final String NAME = "GoThrift-Thread";

    private TProcessor processor;
    private int port;
    private int selectorThreads;
    private int workerThreads;

    private volatile TServer server;

    public ServiceThread(TMultiplexedProcessor processor, int port, int selectorThreads, int workerThreads) {
        super(NAME);
        this.processor = processor;
        this.port = port;
        this.selectorThreads = selectorThreads;
        this.workerThreads = workerThreads;
    }

    public void run() {
        try {
            TNonblockingServerSocket serverSocket = new TNonblockingServerSocket(port);
            TThreadedSelectorServer.Args args = new TThreadedSelectorServer.Args(serverSocket);
            args.processor(processor);
            args.transportFactory(new TFramedTransport.Factory());
            args.protocolFactory(new TCompactProtocol.Factory());
            args.workerThreads(workerThreads);
            args.selectorThreads(selectorThreads);

            server = new TThreadedSelectorServer(args);
            server.serve();
        } catch (TTransportException e) {
            logger.error("Server start up error, error message is {}", e.getMessage(), e);
            throw new IllegalStateException("Could not create ServerSocket on port " + port, e);
        }
    }

    public boolean isServing() {
        return server != null && server.isServing();
    }

    public void stopServing() {
        if (server != null) {
            server.stop();
        }
    }
}
