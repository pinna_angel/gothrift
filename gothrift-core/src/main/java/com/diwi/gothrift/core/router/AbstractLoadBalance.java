package com.diwi.gothrift.core.router;

import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author dingwei
 * @since 16/4/12.
 */
public abstract class AbstractLoadBalance implements LoadBalance {
    protected Logger logger = LoggerFactory.getLogger(getClass());

    @Override
    public <T> T select(List<T> list) {
        if (CollectionUtils.isEmpty(list)) {
            return null;
        }

        if (list.size() == 1) {
            return list.get(0);
        }

        return doSelect(list);
    }

    protected abstract <T> T doSelect(List<T> list);
}
