package com.diwi.gothrift.core.registry;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.base.Preconditions;
import com.diwi.gothrift.core.zookeeper.NodeListener;

/**
 * @author dingwei
 * @since 16/3/29.
 */
public abstract class AbstractRegistry implements Registry {
    protected final Logger logger = LoggerFactory.getLogger(getClass());

    private String registryUrl;

    private static final byte[] PRESENT = new byte[0];

    private final ConcurrentMap<String, Object> registered = new ConcurrentHashMap<String, Object>();

    private final ConcurrentMap<String, NodeListener> subscribed = new ConcurrentHashMap<String, NodeListener>();

    public String getRegistryUrl() {
        return registryUrl;
    }

    public void setRegistryUrl(String registryUrl) {
        Preconditions.checkNotNull(registryUrl, "registry url == null");
        this.registryUrl = registryUrl;
    }

    @Override
    public void register(String path) {
        Preconditions.checkNotNull(path, "register url == null");

        logger.info("Register: {}", path);

        registered.put(path, PRESENT);

        doRegister(path);
    }

    @Override
    public void unregister(String path) {
        Preconditions.checkNotNull(path, "register url == null");

        logger.info("Unregister: {}", path);

        registered.remove(path);

        doUnregister(path);
    }

    @Override
    public List<String> subscribe(String path, NodeListener listener) {
        Preconditions.checkNotNull(path, "register url == null");
        Preconditions.checkNotNull(listener, "subscribe listener == null");

        logger.info("Subscribe: {}", path);

        subscribed.put(path, listener);

        return doSubscribe(path, listener);
    }

    @Override
    public void unsubscribe(String path) {
        Preconditions.checkNotNull(path, "register url == null");

        logger.info("Unsubscribe: {}", path);

        subscribed.remove(path);

        doUnsubscribe(path);
    }

    protected abstract void doRegister(String path);

    protected abstract void doUnregister(String path);

    protected abstract List<String> doSubscribe(String path, NodeListener nodeListener);

    protected abstract void doUnsubscribe(String path);

    public void recover() {
        // register
        Set<String> recoverRegistered = new HashSet<String>(registered.keySet());
        for (String path : recoverRegistered) {
            register(path);
        }

        // subscribe
        Map<String, NodeListener> recoverSubscribed = new HashMap<String, NodeListener>(subscribed);
        for (Map.Entry<String, NodeListener> entry : recoverSubscribed.entrySet()) {
            NodeListener listener = entry.getValue();
            if (listener != null) {
                subscribe(entry.getKey(), listener);
            }
        }
    }
}
