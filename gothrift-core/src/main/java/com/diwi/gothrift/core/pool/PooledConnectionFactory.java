package com.diwi.gothrift.core.pool;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.pool2.KeyedPooledObjectFactory;
import org.apache.commons.pool2.ObjectPool;
import org.apache.commons.pool2.PooledObject;
import org.apache.commons.pool2.impl.DefaultPooledObject;
import org.apache.thrift.transport.TSocket;

/**
 * @author dingwei
 * @since 16/4/7.
 */
public class PooledConnectionFactory implements KeyedPooledObjectFactory<String, TSocket> {
    protected volatile ObjectPool pool = null;

    private int timeout;

    PooledConnectionFactory() {
        this(1000);
    }

    PooledConnectionFactory(int timeout) {
        this.timeout = timeout;
    }

    @Override
    public PooledObject<TSocket> makeObject(String key) throws Exception {
        int index;
        if ((index = key.indexOf("?")) > -1) {
            key = key.substring(0, index);
        }

        String[] urls = StringUtils.split(key, ":");
        String host = urls[0];
        int port = Integer.parseInt(urls[1]);
        TSocket tSocket = new TSocket(host, port, timeout);
        tSocket.open();

        return new DefaultPooledObject<TSocket>(tSocket);
    }

    @Override
    public void destroyObject(String key, PooledObject<TSocket> p) throws Exception {
        if (p != null && p.getObject() != null) {
            p.getObject().close();
        }
    }

    @Override
    public boolean validateObject(String key, PooledObject<TSocket> p) {
        return p != null && p.getObject() != null && p.getObject().isOpen();
    }

    @Override
    public void activateObject(String key, PooledObject<TSocket> p) throws Exception {

    }

    @Override
    public void passivateObject(String key, PooledObject<TSocket> p) throws Exception {

    }
}
