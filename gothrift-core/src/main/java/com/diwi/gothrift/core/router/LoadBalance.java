package com.diwi.gothrift.core.router;

import java.util.List;

/**
 * Load balance strategy
 * 
 * <pre>
 * If loadbalance is not configured by the client side, then the server side is preferred,
 * if is not configured by the sever side as well, then the RANDOM strategy is used by default.
 * </pre>
 * 
 * @author dingwei
 * @since 16/4/12.
 */
public interface LoadBalance {
    <T> T select(List<T> list);
}
