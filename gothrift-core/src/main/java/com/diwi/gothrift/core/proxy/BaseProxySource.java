package com.diwi.gothrift.core.proxy;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.SocketException;

import com.diwi.gothrift.core.common.URL;
import com.diwi.gothrift.core.config.ReferenceBean;
import com.diwi.gothrift.core.pool.ThriftConnectionPool;
import org.apache.thrift.protocol.TCompactProtocol;
import org.apache.thrift.protocol.TMultiplexedProtocol;
import org.apache.thrift.protocol.TProtocol;
import org.apache.thrift.transport.TFramedTransport;
import org.apache.thrift.transport.TSocket;
import org.apache.thrift.transport.TTransportException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.diwi.gothrift.core.router.RoutingService;
import com.diwi.gothrift.core.utils.Constants;

/**
 * @author dingwei
 * @since 16/5/26.
 */
public abstract class BaseProxySource<T> {
    protected Logger logger = LoggerFactory.getLogger(getClass());

    private String serviceName;
    protected String version;
    private String serviceKey;
    private String loadbalance;
    private int timeout;
    private ThriftConnectionPool pool;

    BaseProxySource(ReferenceBean referenceBean) {
        this.serviceName = referenceBean.getInterface();
        this.version = referenceBean.getVersion();
        this.serviceKey = serviceName + Constants.AND + version;
        this.loadbalance = referenceBean.getLoadbalance();
        this.timeout = referenceBean.getTimeout();
        this.pool = referenceBean.getPool();
    }

    public abstract T createProxy();

    protected Object handle(Method method, Object[] objects) throws IllegalAccessException, InvocationTargetException,
            ClassNotFoundException, NoSuchMethodException, InstantiationException, TTransportException {
        String methodName = method.getName();
        Class<?>[] parameterTypes = method.getParameterTypes();
        if (method.getDeclaringClass() == Object.class) {
            return method.invoke(this, objects);
        }
        if ("toString".equals(methodName) && parameterTypes.length == 0) {
            return this.toString();
        }
        if ("hashCode".equals(methodName) && parameterTypes.length == 0) {
            return this.hashCode();
        }
        if ("equals".equals(methodName) && parameterTypes.length == 1) {
            return this.equals(objects[0]);
        }

        URL url = RoutingService.select(serviceKey, loadbalance);
        if (logger.isDebugEnabled()) {
            logger.debug("the selected provider is {}, param is {}", url, objects[0]);
        }

        if (url == null) {
            logger.error("No provider for service {} has been found.", serviceKey);
            throw new RuntimeException("No provider for service [" + serviceKey + "] has been found.");
        }

        TSocket tSocket = null;
        Integer originalTimeout = null;
        try {
            // logger.info("ACTIVE: {}, IDLE: {}, WAITING: {}", pool.getPool().getNumActivePerKey(),
            // pool.getPool().getNumIdle(url.getAddress()), pool.getPool().getNumWaitersByKey());
            if (pool != null) {
                tSocket = pool.getConnection(url.getAddress());
                if (timeout > 0) {
                    try {
                        originalTimeout = tSocket.getSocket().getSoTimeout();
                    } catch (SocketException e) {
                        logger.warn("A TCP error occurred", e);
                    }
                    tSocket.setTimeout(timeout);
                }
            } else {
                tSocket = new TSocket(url.getHost(), url.getPort(), timeout);
            }

            TFramedTransport transport = new TFramedTransport(tSocket);
            TMultiplexedProtocol protocol = new TMultiplexedProtocol(new TCompactProtocol(transport), url.getName());

            Class<?> clientClass = Class.forName(serviceName + "$Client");
            Constructor<?> constructor = clientClass.getConstructor(TProtocol.class);
            Object instance = constructor.newInstance(protocol);
            if (!transport.isOpen()) {
                transport.open();
            }
            return method.invoke(instance, objects);
        } catch (Throwable ex) {
            logger.error("RPC call ran into exception, service is {}, error message is {}, cause is {}", serviceKey,
                    ex.getMessage(), ex.getCause(), ex);
            if (tSocket != null) {
                tSocket.close();
            }
            throw ex;
        } finally {
            if (tSocket != null) {
                boolean isOpen = tSocket.isOpen();
                if (isOpen && originalTimeout != null) {
                    tSocket.setTimeout(originalTimeout);
                }
                if (pool != null) {
                    pool.returnConnection(url.getAddress(), tSocket);
                } else {
                    if (isOpen) {
                        tSocket.close();
                    }
                }
            }
        }
    }
}
