package com.diwi.gothrift.core.config;

/**
 * @author dingwei
 * @since 16/3/21.
 */
public class RegistryConfig extends AbstractConfig {
    private static final long serialVersionUID = -3985677656507870214L;

    // 注册中心地址
    private String address;

    // 注册中心缺省端口
    private Integer port;

    private String protocol;

    // 注册中心登录用户名
    private String username;
    // 注册中心登录密码
    private String password;

    private String group;

    private String check;
    private Boolean isDefault;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getCheck() {
        return check;
    }

    public void setCheck(String check) {
        this.check = check;
    }

    public Boolean getDefault() {
        return isDefault;
    }

    public void setDefault(Boolean aDefault) {
        isDefault = aDefault;
    }
}
