package com.diwi.gothrift.core.registry;

import java.util.List;

import com.diwi.gothrift.core.zookeeper.NodeListener;

/**
 * 注册中心
 * 
 * @author dingwei
 * @since 16/3/24.
 */
public interface Registry {
    /**
     * 注册数据
     *
     * @param path 注册信息,不允许为空
     */
    void register(String path);

    /**
     * 取消注册
     *
     * @param path 注册信息,不允许为空
     */
    void unregister(String path);

    /**
     * 订阅符合条件的已注册数据,当有注册数据变更时自动推送
     * 
     * @param path 订阅路径
     * @param listener 监听器
     * @return 指定路径的子节点的名称
     */
    List<String> subscribe(String path, NodeListener listener);

    /**
     * 取消订阅
     * 
     * @param path 订阅路径
     *
     */
    void unsubscribe(String path);
}
