package com.diwi.gothrift.core.config;

/**
 * @author dingwei
 * @since 16/3/21.
 */
public class ApplicationConfig extends AbstractConfig {
    private static final long serialVersionUID = 4971612521700271397L;

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
