package com.diwi.gothrift.core.router;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/**
 * 轮询 Round-Robin
 * 
 * @author dingwei
 * @since 16/4/12.
 */
public class RoundRobinLoadBalance extends AbstractLoadBalance {
    private final AtomicLong atomicLong = new AtomicLong(0);

    public static final String NAME = "roundrobin";

    private RoundRobinLoadBalance() {
    }

    private static class Builder {
        private static final RoundRobinLoadBalance instance = new RoundRobinLoadBalance();

        static LoadBalance build() {
            return instance;
        }
    }

    public static LoadBalance getInstance() {
        return Builder.build();
    }

    @Override
    public <T> T doSelect(List<T> list) {
        long index = atomicLong.getAndIncrement();
        return list.get((int) (index % list.size()));
    }
}
