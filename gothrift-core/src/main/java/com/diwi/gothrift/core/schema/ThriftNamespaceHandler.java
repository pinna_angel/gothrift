package com.diwi.gothrift.core.schema;

import com.diwi.gothrift.core.config.*;
import org.springframework.beans.factory.xml.NamespaceHandlerSupport;

/**
 * @author dingwei
 * @since 16/3/21.
 */
public class ThriftNamespaceHandler extends NamespaceHandlerSupport {
    public void init() {
        registerBeanDefinitionParser("application", new ThriftBeanDefinitionParser(ApplicationConfig.class, true));
        registerBeanDefinitionParser("registry", new ThriftBeanDefinitionParser(RegistryConfig.class, true));
        registerBeanDefinitionParser("protocol", new ThriftBeanDefinitionParser(ProtocolConfig.class, true));
        registerBeanDefinitionParser("service", new ThriftBeanDefinitionParser(ServiceBean.class, true));
        registerBeanDefinitionParser("reference", new ThriftBeanDefinitionParser(ReferenceBean.class, true));
    }
}
