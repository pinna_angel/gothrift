package com.diwi.gothrift.core.config;

/**
 * @author dingwei
 * @since 16/3/21.
 */
public class ProtocolConfig extends AbstractConfig {
    private static final long serialVersionUID = -473762573594768131L;

    private String name;
    private String host;
    private Integer port;
    private int selectorThreads = 100;
    private int workerThreads = 0;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public int getSelectorThreads() {
        return selectorThreads;
    }

    public void setSelectorThreads(int selectorThreads) {
        this.selectorThreads = selectorThreads;
    }

    public int getWorkerThreads() {
        return workerThreads;
    }

    public void setWorkerThreads(int workerThreads) {
        this.workerThreads = workerThreads;
    }
}
