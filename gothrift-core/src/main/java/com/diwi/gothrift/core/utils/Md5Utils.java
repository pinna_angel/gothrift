package com.diwi.gothrift.core.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * @author dingwei
 * @since 16/4/12.
 */
public class Md5Utils {
    private static ThreadLocal<MessageDigest> md5Local = new ThreadLocal<MessageDigest>();

    /**
     * Get the md5 of the given key.
     * 
     * @param k key
     * @return the array of bytes for the resulting hash value.
     */
    public static byte[] computeMd5(String k) {
        MessageDigest md5 = md5Local.get();
        if (md5 == null) {
            try {
                md5 = MessageDigest.getInstance("MD5");
                md5Local.set(md5);
            } catch (NoSuchAlgorithmException e) {
                throw new RuntimeException("MD5 not supported", e);
            }
        }
        md5.reset();
        md5.update(ByteUtils.getBytes(k));
        return md5.digest();
    }
}
