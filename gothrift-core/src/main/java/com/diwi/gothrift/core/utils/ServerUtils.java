package com.diwi.gothrift.core.utils;

import static com.diwi.gothrift.core.utils.Constants.DEFAULT_PORT;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.diwi.gothrift.core.config.ProtocolConfig;

/**
 * @author dingwei
 * @since 16/4/29.
 */
public class ServerUtils {
    private static final Logger logger = LoggerFactory.getLogger(ServerUtils.class);

    /**
     * 获得本机的IP地址和服务监听的端口号
     * 
     * @param protocolConfig
     * @return 服务的地址及端口号
     */
    public static String getServiceLocation(ProtocolConfig protocolConfig) {
        Integer port = protocolConfig.getPort();
        if (port == null || port <= 0) {
            port = NetUtils.getAvailablePort(DEFAULT_PORT);
            protocolConfig.setPort(port);
            logger.warn("Use random available port(" + port + ") for thrift service to listen on.");
        }
        String host = NetUtils.getLocalAddress().getHostAddress();
        return "/" + host + ":" + port;
    }

}
