package com.diwi.gothrift.core.common;

import java.io.Serializable;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import com.diwi.gothrift.core.utils.Constants;

/**
 * @author dingwei
 * @since 16/3/30.
 */
public class URL implements Serializable {
    private static final long serialVersionUID = -4872553410508451409L;

    private String protocol;
    private String host;
    private int port;
    private String path;
    private String query;
    private Map<String, String> queryMap;
    private String group;

    public URL(String url) {
        if (url == null || (url = url.trim()).length() == 0) {
            throw new IllegalArgumentException("url is empty");
        }

        int idx = url.indexOf("?");
        if (idx >= 0) {
            query = url.substring(idx + 1);
            queryMap = Constants.AND_SPLITTER.withKeyValueSeparator(Constants.EQUALS).split(query);

            url = url.substring(0, idx);
        }

        idx = url.indexOf("://");
        if (idx >= 0) {
            if (idx == 0) {
                throw new IllegalStateException("url missing protocol: " + url);
            }
            protocol = url.substring(0, idx);
            url = url.substring(idx + 3);
        }

        idx = url.indexOf("/");
        if (idx >= 0) {
            path = url.substring(idx);
            url = url.substring(0, idx);
        }

        idx = url.indexOf(":");
        if (idx >= 0) {
            host = url.substring(0, idx);
            port = Integer.parseInt(url.substring(idx + 1));
        } else if (url.length() > 0) {
            host = url;
        }
    }

    public URL(String url, String group) {
        this(url);
        this.group = group;
    }

    public String getAddress() {
        return port <= 0 ? host : host + ":" + port;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public int getPort() {
        return port;
    }

    public void setPort(int port) {
        this.port = port;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public int getWeight() {
        return MapUtils.getInteger(queryMap, "weight", 1);
    }

    public String getName() {
        return MapUtils.getString(queryMap, "name");
    }

    public String getLoadBalance() {
        return MapUtils.getString(queryMap, "loadbalance");
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)
            return true;

        if (o == null || getClass() != o.getClass())
            return false;

        URL url = (URL) o;

        return new EqualsBuilder().append(port, url.port).append(protocol, url.protocol).append(host, url.host)
                .append(path, url.path).append(query, url.query).append(group, url.group).isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(17, 37).append(protocol).append(host).append(port).append(path).append(query)
                .append(group).toHashCode();
    }

    @Override
    public String toString() {
        StringBuilder buf = new StringBuilder();
        if (StringUtils.isNotEmpty(protocol)) {
            buf.append(protocol).append("://");
        }

        if (StringUtils.isNotEmpty(host)) {
            buf.append(host);
            if (port > 0) {
                buf.append(":");
                buf.append(port);
            }
        }

        if (StringUtils.isNotEmpty(path)) {
            buf.append(path);
        }

        boolean hasParams = StringUtils.isNotEmpty(query);
        if (hasParams) {
            buf.append("?").append(query);
        }

        if (StringUtils.isNotEmpty(group)) {
            buf.append(hasParams ? "&" : "?").append("group=").append(group);
        }

        return buf.toString();
    }
}
