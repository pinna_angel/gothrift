package com.diwi.gothrift.core.router;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArrayList;

import com.diwi.gothrift.core.common.URL;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang3.StringUtils;

import com.google.common.collect.Lists;
import com.google.common.collect.Sets;

/**
 * @author dingwei
 * @since 16/4/11.
 */
public class RoutingService {
    private static final ConcurrentMap<String, CopyOnWriteArrayList<URL>> AVAILABLE_SERVICES = new ConcurrentHashMap<String, CopyOnWriteArrayList<URL>>();

    private static final ConcurrentMap<String, Object> LOCK_MAP = new ConcurrentHashMap<>();

    public static void on(String serviceName, URL address) {
        if (StringUtils.isEmpty(address.getAddress())) {
            return;
        }

        CopyOnWriteArrayList<URL> providers = AVAILABLE_SERVICES.get(serviceName);
        // to avoid duplication
        if (providers != null && providers.contains(address)) {
            return;
        }

        on(serviceName, Sets.<URL> newHashSet(address));
    }

    public static void on(String serviceName, Set<URL> addresses) {
        synchronized (getLock(serviceName)) {
            CopyOnWriteArrayList<URL> urls = AVAILABLE_SERVICES.get(serviceName);
            if (urls != null) {
                addresses.addAll(urls);
            }

            List<URL> list = Lists.newArrayList();
            for (URL address : addresses) {
                int weight = address.getWeight(); // 权重 weight
                while (weight-- > 0) {
                    list.add(address);
                }
            }
            Collections.shuffle(list);
            AVAILABLE_SERVICES.put(serviceName, new CopyOnWriteArrayList<URL>(list));
        }
    }

    private static Object getLock(String serviceName) {
        Object lock = LOCK_MAP.get(serviceName);
        if (lock == null) {
            LOCK_MAP.putIfAbsent(serviceName, new Object());
            lock = LOCK_MAP.get(serviceName);
        }
        return lock;
    }

    public static void off(String serviceName, URL address) {
        CopyOnWriteArrayList<URL> providers = AVAILABLE_SERVICES.get(serviceName);
        if (providers != null) {
            providers.removeAll(Arrays.asList(address));
        }
    }

    public static List<URL> availableServices(String serviceKey) {
        CopyOnWriteArrayList<URL> list = AVAILABLE_SERVICES.get(serviceKey);
        if (list != null) {
            return Collections.unmodifiableList(list);
        }
        return Collections.emptyList();
    }

    public static URL select(String serviceKey, String loadBalance) {
        List<URL> list = availableServices(serviceKey);
        if (CollectionUtils.isEmpty(list)) {
            return null;
        }

        return doSelect(list, loadBalance);
    }

    private static URL doSelect(List<URL> list, String loadbalance) {
        // if loadbalance is not configured by the client side, then the server side is preferred,
        // if it is not configured by the sever side as well, then RANDOM strategy is used by default.
        loadbalance = StringUtils.isEmpty(loadbalance) ? list.get(0).getLoadBalance() : loadbalance;

        LoadBalance loadBalance;
        if (loadbalance == null || loadbalance.equalsIgnoreCase(RandomLoadBalance.NAME)) {
            loadBalance = RandomLoadBalance.getInstance();
        } else if (loadbalance.equalsIgnoreCase(RoundRobinLoadBalance.NAME)) {
            loadBalance = RoundRobinLoadBalance.getInstance();
        } else {
            loadBalance = RandomLoadBalance.getInstance();
        }
        return loadBalance.select(list);
    }

}
