package com.diwi.gothrift.core.proxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

import com.diwi.gothrift.core.config.ReferenceBean;

/**
 * @author dingwei
 * @since 16/5/26.
 */
public class JdkProxySource extends BaseProxySource implements InvocationHandler {
    private final ClassLoader classLoader;
    private final Class<?>[] interfaces;

    public JdkProxySource(ClassLoader classLoader, Class<?>[] interfaces, ReferenceBean referenceBean) {
        super(referenceBean);
        this.classLoader = classLoader;
        this.interfaces = interfaces;
    }

    @Override
    public Object createProxy() {
        return Proxy.newProxyInstance(classLoader, interfaces, this);
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        return handle(method, args);
    }
}
