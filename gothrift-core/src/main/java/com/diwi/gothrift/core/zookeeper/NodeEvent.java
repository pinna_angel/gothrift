package com.diwi.gothrift.core.zookeeper;

/**
 * @author dingwei
 * @since 16/3/30.
 */
public class NodeEvent {
    public enum Type {
        /**
         * A child was added to the path, related to
         * {@link org.apache.curator.framework.recipes.cache.PathChildrenCacheEvent.Type#CHILD_ADDED}
         */
        CHILD_ADDED,

        /**
         * A child's data was changed, related to
         * {@link org.apache.curator.framework.recipes.cache.PathChildrenCacheEvent.Type#CHILD_UPDATED}
         */
        CHILD_UPDATED,

        /**
         * A child was removed from the path, related to
         * {@link org.apache.curator.framework.recipes.cache.PathChildrenCacheEvent.Type#CHILD_REMOVED}
         */
        CHILD_REMOVED
    }

    private String path;
    private Type type;

    public NodeEvent(String path, Type type) {
        this.path = path;
        this.type = type;
    }

    public String getPath() {
        return this.path;
    }

    public Type getType() {
        return type;
    }
}
