package com.diwi.gothrift.core.zookeeper;

import java.util.List;

import org.apache.curator.framework.recipes.cache.ChildData;

/**
 * zookeeper 客户端
 * 
 * @author dingwei
 * @since 16/3/25.
 */
public interface ZookeeperClient {
    /**
     * 创建节点 node creating
     * 
     * @param path 节点路径
     * @param ephemeral 是否为临时路径
     */
    void create(String path, boolean ephemeral);

    /**
     * 删除节点 node removing
     * 
     * @param path 节点路径
     */
    void delete(String path);

    /**
     * 添加zk连接的监听器 listen for zk conn's state changing
     * 
     * @param listener 监听器
     */
    void addStateListener(StateListener listener);

    void removeStateListener(StateListener listener);

    /**
     * 获取指定节点的子节点列表 get children of the node with specified path
     * 
     * @param path 指定路径的节点
     * @return 子节点列表
     */
    List<String> getChildren(String path);

    /**
     * 子节点添加监听器
     * 
     * @param path 指定路径的节点
     * @param nodeListener 监听器
     * @return 指定节点的子节点列表
     */
    List<ChildData> addChildListener(String path, NodeListener nodeListener);

    /**
     * 子节点移除监听器
     * 
     * @param path 指定路径的节点
     *
     */
    void removeChildListener(String path);

    boolean isConnected();

    void close();
}
