package com.diwi.gothrift.core.spring;

import static com.diwi.gothrift.core.utils.Constants.SLASH;

import java.lang.reflect.Constructor;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.thrift.TMultiplexedProcessor;
import org.apache.thrift.TProcessor;
import org.springframework.beans.BeanUtils;
import org.springframework.context.ApplicationContext;

import com.google.common.base.Preconditions;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.diwi.gothrift.core.annotation.GoService;
import com.diwi.gothrift.core.config.AbstractServiceConfig;
import com.diwi.gothrift.core.config.ProtocolConfig;
import com.diwi.gothrift.core.config.RegistryConfig;
import com.diwi.gothrift.core.config.ServiceBean;
import com.diwi.gothrift.core.utils.ServerUtils;

/**
 * annotation-based service registering and publishing
 * 
 * @author dingwei
 * @since 16/3/31.
 */
public class ServicePublisher extends AbstractServiceConfig {
    private static final long serialVersionUID = -6774844875578267604L;

    @Override
    protected boolean canPublish() {
        // xml-based configuration is prior to annotation-based configuration
        return MapUtils.isEmpty(CONTEXT.getBeansOfType(ServiceBean.class));
    }

    protected void publish() {
        logger.info("Initializing and exporting thrift service.");
        ApplicationContext context = CONTEXT;

        // get & check configurations
        Map<String, Object> beanMap = context.getBeansWithAnnotation(GoService.class);
        if (MapUtils.isEmpty(beanMap)) {
            logger.info("no thrift service found.");
            return;
        }

        final ProtocolConfig protocolConfig = context.getBean(ProtocolConfig.class);
        if (protocolConfig == null) {
            logger.warn("protocolConfig has not been configured.");
            return;
        }
        ServicePublisher.protocolConfig = protocolConfig;

        Map<String, RegistryConfig> registryMap = context.getBeansOfType(RegistryConfig.class);
        if (MapUtils.isEmpty(registryMap)) {
            logger.warn("registry has not been configured.");
            return;
        }

        // thrift server start serving
        final String location = ServerUtils.getServiceLocation(protocolConfig);
        doStartService(protocolConfig.getPort());

        // service registering
        StringBuilder buff;
        for (Object serviceObj : beanMap.values()) {
            Class<?> serviceClass = serviceObj.getClass();
            GoService annotation = serviceClass.getAnnotation(GoService.class);
            if (annotation == null) {
                continue;
            }
            buff = new StringBuilder(SLASH).append(annotation.iface().getName()).append(SLASH)
                    .append(annotation.version());
            String serviceRoot = buff.toString();

            buff.append(location).append("?name=").append(annotation.name());

            // 负载均衡策略 load balance strategy
            if (StringUtils.isNotEmpty(annotation.loadbalance())) {
                buff.append("&loadbalance=").append(annotation.loadbalance());
            }

            // 权重 weight
            buff.append("&weight=").append(annotation.weight());

            String serviceNode = buff.toString();
            SERVICE_NODE.put(serviceNode, serviceRoot);

            String[] registryIds = annotation.registry();
            List<RegistryConfig> registries = Lists.newArrayList();
            if (ArrayUtils.isEmpty(registryIds)) {
                registries.addAll(registryMap.values());
            } else {
                for (String registryId : registryIds) {
                    RegistryConfig config = registryMap.get(registryId);
                    if (config == null) {
                        logger.error("Group[{}] not exist, service class is {}", registryId, serviceClass);
                        continue;
                    }
                    registries.add(config);
                }
            }

            registerAndSubscribe(registries, serviceNode, serviceRoot, location);
        }
    }

    protected TMultiplexedProcessor getProcessor() {
        Map<String, Object> beanMap = CONTEXT.getBeansWithAnnotation(GoService.class);
        final TMultiplexedProcessor tMultiplexedProcessor = new TMultiplexedProcessor();

        Map<String, TProcessor> serviceMap = Maps.newHashMap();
        for (Map.Entry<String, Object> entry : beanMap.entrySet()) {
            String name = entry.getKey();
            Object bean = entry.getValue();

            GoService annotation = bean.getClass().getAnnotation(GoService.class);
            String serviceName = StringUtils.isEmpty(annotation.name()) ? name : annotation.name();
            Class<?> processorClass = annotation.processor();
            Class<?> ifaceClass = annotation.iface();

            if (TProcessor.class.isAssignableFrom(processorClass)) {
                Constructor<?> constructor;
                try {
                    constructor = processorClass.getConstructor(ifaceClass);
                } catch (NoSuchMethodException e) {
                    logger.error("missing interface, service is {}", serviceName);
                    continue;
                }

                TProcessor processor = (TProcessor) BeanUtils.instantiateClass(constructor, bean);
                tMultiplexedProcessor.registerProcessor(serviceName, processor);

                serviceMap.put(serviceName, processor);
            } else {
                logger.error("{} type error", serviceName);
            }
        }

        Preconditions.checkArgument(MapUtils.isNotEmpty(serviceMap), "thrift service configurations error.");
        return tMultiplexedProcessor;
    }

}
