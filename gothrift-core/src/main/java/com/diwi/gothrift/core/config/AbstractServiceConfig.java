package com.diwi.gothrift.core.config;

import static com.diwi.gothrift.core.utils.Constants.REGISTRY_SPLIT_PATTERN;

import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.TimeUnit;

import com.diwi.gothrift.core.common.ServiceThread;
import com.diwi.gothrift.core.registry.CuratorZookeeperRegistry;
import com.diwi.gothrift.core.registry.Registry;
import com.diwi.gothrift.core.zookeeper.NodeEvent;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.thrift.TMultiplexedProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.context.event.ContextRefreshedEvent;

import com.diwi.gothrift.core.common.URL;
import com.diwi.gothrift.core.zookeeper.NodeListener;

/**
 * abstract service config, providing basic init and destroy method
 * <p>
 * xml-based configuration is <b>PRIOR</b> to annotation-based configuration
 * 
 * @author dingwei
 * @since 16/5/4.
 */
public abstract class AbstractServiceConfig extends AbstractConfig
        implements InitializingBean, ApplicationListener, ApplicationContextAware {
    private static final long serialVersionUID = 5480176757750802013L;

    private static final Logger log = LoggerFactory.getLogger(AbstractServiceConfig.class);

    // delay exporting
    protected Integer delay;

    protected static transient ApplicationContext CONTEXT;

    protected static ServiceThread servingThread;

    protected static ProtocolConfig protocolConfig;

    // cache to unregister
    protected static ConcurrentMap<String, String> SERVICE_NODE = new ConcurrentHashMap<String, String>();

    static {
        // shutdown hook to unregister service
        Runtime.getRuntime().addShutdownHook(new Thread(new Runnable() {
            @Override
            public void run() {
                log.info("Run shutdown hook now.");
                try {
                    destroy();
                } catch (Exception e) {
                    log.error("Run shutdown hook failed", e);
                }
            }
        }, "GoThriftShutdownHook"));
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        if (applicationContext != null) {
            CONTEXT = applicationContext;
        }
    }

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        String eventName = event.getClass().getName();

        if (ContextRefreshedEvent.class.getName().equals(eventName)) {
            if (canPublish()) {
                publish();
            }
        } else if (ContextClosedEvent.class.getName().equalsIgnoreCase(eventName)) {
            destroy();
        }
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        init();
    }

    protected void init() {
        // do nothing at all
    }

    private static void destroy() {
        if (CONTEXT == null || SERVICE_NODE.isEmpty()) {
            log.warn("destroy fail, {}, {}", CONTEXT, SERVICE_NODE);
            return;
        }

        log.info("Closing thrift service.");

        Map<String, RegistryConfig> registryMap = CONTEXT.getBeansOfType(RegistryConfig.class);

        for (RegistryConfig registryConfig : registryMap.values()) {
            String[] addresses = REGISTRY_SPLIT_PATTERN.split(registryConfig.getAddress());
            for (String address : addresses) {
                Registry registry = CuratorZookeeperRegistry.getRegistry(new URL(address, registryConfig.getGroup()));

                for (Map.Entry<String, String> entry : SERVICE_NODE.entrySet()) {
                    String serviceNode = entry.getKey();
                    String serviceRoot = entry.getValue();
                    registry.unsubscribe(serviceRoot);
                    registry.unregister(serviceNode);
                }
            }
        }

        if (servingThread != null) {
            servingThread.stopServing();
        }
    }

    protected abstract boolean canPublish();

    protected abstract void publish();

    protected void doStartService(Integer port) {
        ServiceThread serviceThread = new ServiceThread(getProcessor(), port, protocolConfig.getSelectorThreads(),
                protocolConfig.getWorkerThreads());
        serviceThread.setDaemon(true);
        serviceThread.start();

        while (!serviceThread.isServing()) {
            try {
                TimeUnit.MILLISECONDS.sleep(100);
            } catch (InterruptedException e) {
                logger.warn("waiting for server starting is interrupted.");
            }
        }

        logger.info("Thrift service has started successfully.");
        servingThread = serviceThread;
    }

    protected abstract TMultiplexedProcessor getProcessor();

    protected void registerAndSubscribe(List<RegistryConfig> registries, String serviceNode, String serviceRoot,
            final String location) {
        for (RegistryConfig config : registries) {
            String addr = config.getAddress();
            String[] addresses;
            if (StringUtils.isEmpty(addr) || ArrayUtils.isEmpty(addresses = REGISTRY_SPLIT_PATTERN.split(addr))) {
                continue;
            }

            for (String address : addresses) {
                final Registry registry = CuratorZookeeperRegistry.getRegistry(new URL(address, config.getGroup()));
                registry.register(serviceNode);
                registry.subscribe(serviceRoot, new NodeListener() {
                    public void nodeChanged(NodeEvent event) {
                        if (event.getType() == NodeEvent.Type.CHILD_REMOVED) {
                            if (StringUtils.contains(event.getPath(), location)) {
                                registry.register(event.getPath());
                            }
                        }
                    }
                });
            }
        }
    }

    public Integer getDelay() {
        return delay;
    }

    public void setDelay(Integer delay) {
        this.delay = delay;
    }
}
