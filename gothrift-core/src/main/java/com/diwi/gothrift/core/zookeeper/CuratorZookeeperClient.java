package com.diwi.gothrift.core.zookeeper;

import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.CopyOnWriteArraySet;

import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.curator.framework.CuratorFramework;
import org.apache.curator.framework.CuratorFrameworkFactory;
import org.apache.curator.framework.imps.CuratorFrameworkState;
import org.apache.curator.framework.recipes.cache.ChildData;
import org.apache.curator.framework.recipes.cache.PathChildrenCache;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheEvent;
import org.apache.curator.framework.recipes.cache.PathChildrenCacheListener;
import org.apache.curator.framework.state.ConnectionState;
import org.apache.curator.framework.state.ConnectionStateListener;
import org.apache.curator.retry.RetryNTimes;
import org.apache.zookeeper.CreateMode;
import org.apache.zookeeper.KeeperException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.alibaba.fastjson.JSON;
import com.google.common.base.Preconditions;
import com.google.common.collect.Maps;
import com.diwi.gothrift.core.common.URL;

/**
 * Curator/zookeeper client
 * 
 * @author dingwei
 * @since 16/3/25.
 */
public class CuratorZookeeperClient implements ZookeeperClient {
    private static final Logger LOGGER = LoggerFactory.getLogger(CuratorZookeeperClient.class);

    private final CuratorFramework client;

    private Set<StateListener> stateListeners = new CopyOnWriteArraySet<StateListener>();

    private ConcurrentMap<String, ImmutablePair<PathChildrenCache, PathChildrenCacheListener>> childNodeCacheMap = Maps
            .newConcurrentMap();

    public CuratorZookeeperClient(URL url) {
        CuratorFrameworkFactory.Builder builder = CuratorFrameworkFactory.builder().connectString(url.getAddress())
                .retryPolicy(new RetryNTimes(Integer.MAX_VALUE, 1000)).connectionTimeoutMs(5000);
        if (StringUtils.isNotEmpty(url.getGroup())) {
            builder.namespace(url.getGroup());
        }

        client = builder.build();

        // listen for zk conn's state changing, and curator will try to reconnect to zk automatically.
        client.getConnectionStateListenable().addListener(new ConnectionStateListener() {
            @Override
            public void stateChanged(CuratorFramework client, ConnectionState newState) {
                LOGGER.info("zk client's connection state has been changed, new state is {}", newState);

                // 将zookeeper连接发生变化的回调信息回传给注册中心,以便注册中心重新进行服务注册
                // pass conn's state changing message to registry, then registry can retry to subscribe service.
                if (newState == ConnectionState.RECONNECTED) {
                    // need to recover subscribing
                    CuratorZookeeperClient.this.stateChanged(StateListener.RECONNECTED);
                } else if (newState == ConnectionState.CONNECTED) {
                    CuratorZookeeperClient.this.stateChanged(StateListener.CONNECTED);
                } else if (newState == ConnectionState.LOST) {
                    CuratorZookeeperClient.this.stateChanged(StateListener.DISCONNECTED);
                }
            }
        });
        client.start();
    }

    @Override
    public void create(String path, boolean ephemeral) {
        Preconditions.checkArgument(client.getState() == CuratorFrameworkState.STARTED,
                "zookeeper client has not yet been started.");

        try {
            client.create().creatingParentsIfNeeded().withMode(ephemeral ? CreateMode.EPHEMERAL : CreateMode.PERSISTENT)
                    .forPath(path);
        } catch (KeeperException.NodeExistsException e) {
            LOGGER.warn("node [{}] already exists.", path);
        } catch (Exception e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }

    @Override
    public void delete(String path) {
        Preconditions.checkArgument(client.getState() == CuratorFrameworkState.STARTED,
                "zookeeper client has not yet been started.");

        try {
            client.delete().forPath(path);
        } catch (KeeperException.NoNodeException e) {
            LOGGER.warn("node [{}] already deleted.", path);
        } catch (Exception e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }

    @Override
    public void addStateListener(StateListener listener) {
        stateListeners.add(listener);
    }

    @Override
    public void removeStateListener(StateListener listener) {
        stateListeners.remove(listener);
    }

    private Set<StateListener> getStateListeners() {
        return this.stateListeners;
    }

    @Override
    public List<String> getChildren(String path) {
        Preconditions.checkArgument(client.getState() == CuratorFrameworkState.STARTED,
                "zookeeper client has not yet been started.");

        try {
            return client.getChildren().forPath(path);
        } catch (KeeperException.NoNodeException e) {
            return null;
        } catch (Exception e) {
            throw new IllegalStateException(e.getMessage(), e);
        }
    }

    @Override
    public List<ChildData> addChildListener(final String path, final NodeListener nodeListener) {
        Preconditions.checkArgument(client.getState() == CuratorFrameworkState.STARTED,
                "zookeeper client has not yet been started.");

        ImmutablePair<PathChildrenCache, PathChildrenCacheListener> pair = childNodeCacheMap.get(path);
        if (pair != null) {
            LOGGER.info("path {} has been watched.", pair);
            return pair.getLeft().getCurrentData();
        }

        final PathChildrenCache pathChildrenCache = new PathChildrenCache(client, path, true);
        PathChildrenCacheListener listener = new PathChildrenCacheListener() {
            @Override
            public void childEvent(CuratorFramework client, PathChildrenCacheEvent event) throws Exception {
                LOGGER.info("path change watched, and current children = {}, event = {}",
                        JSON.toJSONString(pathChildrenCache.getCurrentData()), event);
                if (event.getData() == null) {
                    return;
                }

                if (event.getType() == PathChildrenCacheEvent.Type.CHILD_ADDED) {
                    nodeListener.nodeChanged(new NodeEvent(event.getData().getPath(), NodeEvent.Type.CHILD_ADDED));
                } else if (event.getType() == PathChildrenCacheEvent.Type.CHILD_REMOVED) {
                    nodeListener.nodeChanged(new NodeEvent(event.getData().getPath(), NodeEvent.Type.CHILD_REMOVED));
                } else if (event.getType() == PathChildrenCacheEvent.Type.CHILD_UPDATED) {
                    nodeListener.nodeChanged(new NodeEvent(event.getData().getPath(), NodeEvent.Type.CHILD_UPDATED));
                }
            }
        };

        childNodeCacheMap.putIfAbsent(path,
                new ImmutablePair<PathChildrenCache, PathChildrenCacheListener>(pathChildrenCache, listener));

        pair = childNodeCacheMap.get(path);

        pair.getLeft().getListenable().addListener(pair.getRight());

        try {
            pair.getLeft().start(PathChildrenCache.StartMode.BUILD_INITIAL_CACHE);
        } catch (IllegalStateException e) {
            if (!e.getMessage().equals("already started")) {
                throw new IllegalStateException(e.getMessage(), e);
            }
        } catch (Exception e) {
            LOGGER.error("starting pathChildCache error, {}", e.getMessage());
            throw new IllegalStateException(e.getMessage(), e);
        }
        return pair.getLeft().getCurrentData();
    }

    @Override
    public void removeChildListener(String path) {
        ImmutablePair<PathChildrenCache, PathChildrenCacheListener> cache = childNodeCacheMap.get(path);
        if (cache == null) {
            return;
        }

        cache.getLeft().getListenable().removeListener(cache.getRight());
        childNodeCacheMap.remove(path);
    }

    @Override
    public boolean isConnected() {
        return client.getZookeeperClient().isConnected();
    }

    @Override
    public void close() {
        client.close();
    }

    private void stateChanged(int state) {
        for (StateListener stateListener : getStateListeners()) {
            stateListener.stateChanged(state);
        }
    }
}
